FROM eclipse-temurin:11-jdk-alpine
RUN apk --no-cache add curl
RUN apk --no-cache add --update ttf-dejavu
EXPOSE 8080
ARG JAR_FILE
ADD ${JAR_FILE} app.war
COPY ./init-data /tmp/init-data
COPY --chmod=777 ./docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh", "java","-Djavax.net.ssl.trustStore=/opt/data/cacerts/truststore-current-descartes.ks","-Djavax.net.ssl.trustStorePassword=descartes","-jar","/app.war"]
