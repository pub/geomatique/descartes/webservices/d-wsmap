package fr.gouv.siig.descartes.services.descartes.services;

import fr.gouv.siig.descartes.services.descartes.tools.HttpProxy;
import fr.gouv.siig.descartes.services.descartes.tools.HttpSender;
import fr.gouv.siig.descartes.services.descartes.tools.PapersManager;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;

import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;


/**
 * Class: descartes.services.PdfMapWriter
 * Classe du thread de génération PDF.
 * 
 * Implémente:
 * - java.lang.Runnable
 * 
 * Hérite de:
 * - <descartes.services.AbstractMapWriter>
 */
public class PdfMapWriter extends AbstractMapWriter implements Runnable {
	
	private static final float RESOLUTION = 72;

	/**
	 * Constructeur: PdfMapWriter(HttpProxy)
	 * Constructeur d'instances
	 * 
	 * Paramètres:
	 * proxy - {<descartes.tools.HttpProxy>} Proxy.
	 */
	public PdfMapWriter(HttpProxy proxy) {
		super(proxy);
	}

	/**
	 * Methode: run()
	 * Exécute le thread.
	 */
	public void run() {
		try {
			if (this.urlCartes.length + this.urlLegendes.length>0) {
				this.pas = 100/(this.urlCartes.length + this.urlLegendes.length);
			}

			Rectangle page;
			try {
				page = PageSize.getRectangle(this.pageFormat);
			} catch (RuntimeException rte) {
				page = PapersManager.getPaper(this.pageFormat);
			}
			if (page == null) {
				throw new Exception("Format de papier introuvable");
			} else if (this.pageOrientation.equals("L")) {
				page = page.rotate();
			}
			float margeGauche = this.pageMarginLeft * 2.54F;
			float margeDroite = this.pageMarginRight * 2.54F;
			float margeBas = this.pageMarginBottom * 2.54F;
			float margeHaut = this.pageMarginTop * 2.54F;
			float interLigne = 15;
			float hauteurPage = page.getHeight();
			float largeurPage = page.getWidth();
			float pourcentCarte = PdfMapWriter.RESOLUTION;
			float pourcentLogo = PdfMapWriter.RESOLUTION;
			float pourcentLegende = PdfMapWriter.RESOLUTION;
			float hauteurCarte = this.hauteurPixCarte*pourcentCarte/100;
			float largeurCarte = this.largeurPixCarte*pourcentCarte/100;

			Document documentPDF = new Document(page);
			PdfWriter writer = PdfWriter.getInstance(documentPDF, this.getWriter());
	        if (this.creator != null) {
	        	documentPDF.addCreator(this.creator);
	        }
	        if (this.auteurCarte != null) {
	        	documentPDF.addAuthor(this.auteurCarte);
	        }
			documentPDF.addTitle(this.getTitreCarte());
	        if (this.descriptionCarte != null) {
	        	documentPDF.addKeywords(this.descriptionCarte);
	        }
			documentPDF.open();
			this.avancement = 1;
			
			float positionX = margeGauche;
			float positionY = hauteurPage-margeHaut-hauteurCarte; // position en bas à gauche de l'image

			PdfContentByte cb = writer.getDirectContent();
	        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
	        BaseFont bfg = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			
	        cb.beginText();
	        cb.setFontAndSize(bfg, 12);
	        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, this.getTitreCarte(), positionX+(largeurCarte/2), hauteurPage-margeHaut-interLigne, 0);
	        positionY -= interLigne*2;
	        cb.endText();
	        
			BufferedImage imageFinale = new BufferedImage(this.largeurPixCarte,this.hauteurPixCarte,BufferedImage.TYPE_INT_RGB);
			Graphics2D graphicImageFinale = imageFinale.createGraphics();
	
			graphicImageFinale.setColor(Color.WHITE);
			graphicImageFinale.fillRect(0,0,this.largeurPixCarte,this.hauteurPixCarte);
			for (int indexImage=0 ; indexImage<this.urlCartes.length ; indexImage++) {
				if (!this.urlCartes[indexImage].equals("KO")) {
					byte[] dbBytes = HttpSender.getGetResponseAsByte(this.urlCartes[indexImage], this.httpProxy);
					ByteArrayInputStream bais = new ByteArrayInputStream(dbBytes);
					BufferedImage imageCouche = ImageIO.read(bais);
					BufferedImage imageCoucheTemp = new BufferedImage(imageCouche.getWidth(null), imageCouche.getHeight(null), BufferedImage.TYPE_INT_ARGB);
					Graphics graphicImageCoucheTemp = imageCoucheTemp.getGraphics();
					graphicImageCoucheTemp.drawImage(imageCouche, 0, 0, null);
					float[] scales = { 1f, 1f, 1f, this.opacites[indexImage] };
					float[] offsets = new float[4];
					RescaleOp rop = new RescaleOp(scales, offsets, null);
					graphicImageFinale.drawImage(imageCoucheTemp, rop, 0, 0);

					this.avancement += this.pas;
				}
			}
			
			graphicImageFinale.setBackground(Color.WHITE);
			Image carte = Image.getInstance(imageFinale,null);
			carte.scalePercent(pourcentCarte);
			carte.setAbsolutePosition(positionX, positionY);
			cb.addImage(carte);

			cb.setColorStroke(Color.BLACK);
			cb.setLineWidth(1);
			cb.rectangle(positionX, positionY, largeurCarte, hauteurCarte);
			cb.stroke();
			
			positionY += interLigne;
			if (this.longueurEchelle!=0) {
				float longueur = this.longueurEchelle*PdfMapWriter.RESOLUTION/100;
				cb.setLineWidth(1);
				cb.setColorFill(Color.WHITE);
				cb.rectangle(positionX+10, positionY, longueur, 5);
				cb.fillStroke();
				cb.setColorFill(Color.BLACK);
				float petiteLongueur = longueur/4;
				cb.rectangle(positionX+petiteLongueur+10, positionY, petiteLongueur, 5);
				cb.rectangle(positionX+3*petiteLongueur+10, positionY, petiteLongueur, 5);
				cb.fillStroke();

				cb.setFontAndSize(bf, 8);
		        float largeurEchelle = cb.getEffectiveStringWidth(this.affichageEchelle.trim(), false);
		        cb.setColorFill(Color.WHITE);
		        cb.setColorStroke(Color.WHITE);
		        cb.rectangle(positionX+longueur+20, positionY, largeurEchelle, 8);
		        cb.fillStroke();
		        cb.closePathFillStroke();
		        cb.setColorFill(Color.BLACK);
				
				cb.beginText();
				cb.showTextAligned(PdfContentByte.ALIGN_LEFT, this.affichageEchelle, positionX+longueur+20, positionY, 0);
				cb.endText();
			}
			cb.closePathFillStroke();
			
	        if (this.copyrightCarte != null) {
		        cb.setFontAndSize(bf, 8);
		        float largeurCopyright = cb.getEffectiveStringWidth("©" + this.copyrightCarte, false);
		        cb.setColorFill(Color.WHITE);
		        cb.setColorStroke(Color.WHITE);
		        cb.rectangle(positionX+10, hauteurPage-margeHaut-interLigne*3, largeurCopyright, 8);
		        cb.fillStroke();
		        cb.closePathFillStroke();
		        cb.setColorFill(Color.BLACK);
				cb.beginText();
		        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "©" + this.copyrightCarte, positionX+10, hauteurPage-margeHaut-interLigne*3, 0);
				cb.endText();
	        }
			
			cb.beginText();
	        positionY -= interLigne*2;

	        if (this.descriptionCarte != null) {
	        	String[] description = this.descriptionCarte.split("\n");
		        cb.setFontAndSize(bfg, 10);
		        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Description :", positionX, positionY, 0);
		        positionY -= interLigne;
		        cb.setFontAndSize(bf, 10);
		        for (int i=0 ; i<description.length ; i++) {
		        	for (String uneLigne : texteSurPlusieursLignes(description[i])) {
				        cb.showTextAlignedKerned(PdfContentByte.ALIGN_LEFT, uneLigne, positionX, positionY, 0);
			        	positionY -= interLigne;
		        	}
		        }
	        }

	        if (this.applicationInfos != null) {
	        	String[] appInfos = this.applicationInfos.split("\n");
		        cb.setFontAndSize(bfg, 8);
		        for (int i=0 ; i<appInfos.length ; i++) {
			        positionY -= interLigne*2/3;
			        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, appInfos[i], positionX+(largeurCarte/2), positionY, 0);
		        }
	        }

	        cb.endText();
	        
	        float interLogo = 5;
	        float placeLogoX = 0;
	        ArrayList<Image> imgLogos = new ArrayList<Image>();
	        if (this.fileLogos.length !=0 ) {
	        	for (int i=0 ; i<this.fileLogos.length ; i++) {
	        		BufferedImage imgLogoFichier = ImageIO.read(new File(this.fileLogos[i]));
	        		if (imgLogoFichier.getHeight() > this.sizeMaxLogo) {
	        			// il faut diminuer le logo
	        			imgLogoFichier = this.reduireLogo(imgLogoFichier, this.sizeMaxLogo);
	        		}
	        		Image imgLogo = Image.getInstance(imgLogoFichier,null);
		        	imgLogos.add(imgLogo);
		        	placeLogoX += imgLogo.getWidth()*pourcentLogo/100;
	        	}
	        }
	        if (this.urlLogos.length != 0) {
	        	for (int i=0 ; i<this.urlLogos.length ; i++) {
					if (!this.urlLogos[i].equals("KO")) {
						byte[] dbBytes = HttpSender.getGetResponseAsByte(this.urlLogos[i], this.httpProxy);
						BufferedImage imgLogoFichier = ImageIO.read(new ByteArrayInputStream(dbBytes));
						if(imgLogoFichier!= null){
							if (imgLogoFichier.getHeight() > this.sizeMaxLogo) {
			        			// il faut diminuer le logo
								imgLogoFichier = this.reduireLogo(imgLogoFichier, this.sizeMaxLogo);
			        		}
			        		Image imgLogo = Image.getInstance(imgLogoFichier,null);		        		
				        	imgLogos.add(imgLogo);
				        	placeLogoX += imgLogo.getWidth()*pourcentLogo/100;
						}
					}
	        	}
	        }
        	placeLogoX += (imgLogos.size() -1)*interLogo;
	        float resteX = largeurPage - (largeurCarte + interLigne + margeDroite) - placeLogoX;
	        float positionLogoX = resteX / 2;
	        float positionLogoY = 0;
	        positionX += largeurCarte + interLigne;
	        positionY = hauteurPage-margeHaut;
	        for (int i=0 ; i<imgLogos.size() ; i++) {
	        	imgLogos.get(i).scalePercent(pourcentLogo);
		        positionLogoY = hauteurPage-margeHaut-(imgLogos.get(i).getHeight()*pourcentLogo/100);
		        imgLogos.get(i).setAbsolutePosition(positionX + positionLogoX,positionLogoY);
		        positionLogoX += imgLogos.get(i).getWidth()*pourcentLogo/100 +interLogo;
		        documentPDF.add(imgLogos.get(i));
		        if (i==0) {
		        	positionY = positionLogoY;
		        } else if (positionY>positionLogoY) {
		        	positionY = positionLogoY;
		        }
	        }
	        positionY-=interLigne;
	        
	        cb.beginText();
	        positionY -= interLigne;

	        cb.setFontAndSize(bf, 10);
	        if (this.auteurCarte != null) {
		        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Conception : " + this.auteurCarte, positionX, positionY, 0);
		        positionY -= interLigne;
	        }
	        if (this.dateValiditeCarte != null) {
		        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Date de validité : " + this.dateValiditeCarte, positionX, positionY, 0);
		        positionY -= interLigne;
	        }
	        String date = (new SimpleDateFormat("dd-MM-yyyy")).format(new Date());
	        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Date d'impression : " + date, positionX, positionY, 0);
	        positionY -= interLigne*2;
	        cb.endText();
	        
	        for (int i=this.urlLegendes.length-1;i>=0; i--) {
	        	try {
					if (!this.urlLegendes[i].equals("KO")) {
						byte[] dbBytes = HttpSender.getGetResponseAsByte(this.urlLegendes[i], this.httpProxy);
						ByteArrayInputStream bais = new ByteArrayInputStream(dbBytes);
						BufferedImage imageLegendeCouche = ImageIO.read(bais);
						if(imageLegendeCouche!=null){
							BufferedImage imageLegende = new BufferedImage(imageLegendeCouche.getWidth(null),imageLegendeCouche.getHeight(null),BufferedImage.TYPE_INT_ARGB);
							Graphics2D graphicImageLegende = imageLegende.createGraphics();
					
							graphicImageLegende.setColor(Color.WHITE);
							graphicImageLegende.drawImage(imageLegendeCouche, 0, 0, null);
							graphicImageLegende.setBackground(Color.WHITE);
							Image legende = Image.getInstance(imageLegende,null);
	
				        	legende.scalePercent(pourcentLegende);
				        	if ((legende.getHeight()*pourcentLegende/100)<(hauteurPage-margeBas-margeHaut)) {
					        	positionY -= legende.getHeight()*pourcentLegende/100;
					        	
					        	if (positionY < margeBas) {
					        		positionX += legende.getWidth()*pourcentLegende/100;
					        		positionY = hauteurPage-margeHaut-(legende.getHeight()*pourcentLegende/100);
					        		if (positionX+(legende.getWidth()*pourcentLegende/100) > largeurPage-margeDroite) {
					        			documentPDF.newPage();
					        			positionX = margeGauche;
					        			positionY = hauteurPage-margeHaut-(legende.getHeight()*pourcentLegende/100);
					        		}
					        	}
					        	legende.setAbsolutePosition(positionX, positionY);
					    		documentPDF.add(legende);
				        	}
						}
					}
	        	} catch(IOException ex) {
	        		// ne rien faire - passer à la légende suivante
	        	}
	        	this.avancement += this.pas;
	        }
	        
	        documentPDF.close();
	        this.avancement = 100;
		} catch (Exception ex) {
			this.erreur = ex;
			this.avancement = -1;
		}
	}
	
	private ArrayList<String> texteSurPlusieursLignes(String texte) {
		ArrayList<String> lignes = new ArrayList<String>();
		String[] mots = texte.split(" ");
		int nbCaracteres =0;
		String ligne = "";
		for (int indexMot=0 ; indexMot<mots.length ; indexMot++) {
			if ((nbCaracteres + 1 + mots[indexMot].length()) > this.largeurPixCarte/6) {
				lignes.add(ligne);
				ligne = mots[indexMot];
				if (indexMot== mots.length-1) {
					lignes.add(ligne);
				}
				nbCaracteres = 0;
			} else {
				ligne += (ligne == "" ? "" : " ") + mots[indexMot];
				if (indexMot== mots.length-1) {
					lignes.add(ligne);
				}
				nbCaracteres = ligne.length();
			}
		}
		return lignes;
	}
	
	private BufferedImage reduireLogo(BufferedImage imgLogo, int size) {
		BufferedImage newImgLogo = null;
		double ratio = (double) size / ((double) imgLogo.getHeight(null) );
		int largeur = (int) (imgLogo.getWidth(null) * ratio);
		BufferedImage imgLogoReduit = new BufferedImage(largeur, size, BufferedImage.TYPE_INT_ARGB);
		AffineTransform affineTransform = new AffineTransform();
		affineTransform.setToScale(ratio, ratio);
		RenderingHints  rh = new RenderingHints (RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put (RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        rh.put (RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        rh.put (RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        rh.put (RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        rh.put (RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        rh.put (RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		AffineTransformOp op = new AffineTransformOp(affineTransform, rh);
        op.filter(imgLogo, imgLogoReduit);
        newImgLogo = imgLogoReduit;
		return newImgLogo;
	}
	
}
