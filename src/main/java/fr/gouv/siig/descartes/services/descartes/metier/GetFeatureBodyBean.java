package fr.gouv.siig.descartes.services.descartes.metier;

public class GetFeatureBodyBean {
    private String infos;
    private String format;
    private String dataMask;
    private String gmlMask;
    private String pixelMask;
    private String withCsvExport;
    private String withReturn;
    
    
    
	public GetFeatureBodyBean(String infos, String format, String dataMask, String gmlMask, String pixelMask, String withCsvExport,
			String withReturn) {
		super();
		this.infos = infos;
		this.format = format;
		this.dataMask = dataMask;
		this.gmlMask = gmlMask;
		this.pixelMask = pixelMask;
		this.withCsvExport = withCsvExport;
		this.withReturn = withReturn;
	}
	
	public String getInfos() {
		return infos;
	}
	public void setInfos(String infos) {
		this.infos = infos;
	}
	
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getDataMask() {
		return dataMask;
	}
	public void setDataMask(String dataMask) {
		this.dataMask = dataMask;
	}
	public String getGmlMask() {
		return gmlMask;
	}
	public void setGmlMask(String gmlMask) {
		this.gmlMask = gmlMask;
	}
	public String getPixelMask() {
		return pixelMask;
	}
	public void setPixelMask(String pixelMask) {
		this.pixelMask = pixelMask;
	}
	public String getWithCsvExport() {
		return withCsvExport;
	}
	public void setWithCsvExport(String withCsvExport) {
		this.withCsvExport = withCsvExport;
	}
	public String getWithReturn() {
		return withReturn;
	}
	public void setWithReturn(String withReturn) {
		this.withReturn = withReturn;
	}
    
    
}