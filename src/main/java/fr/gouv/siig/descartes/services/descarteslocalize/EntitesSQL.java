package fr.gouv.siig.descartes.services.descarteslocalize;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class EntitesSQL {
	
	private static String database = "descarteslocalize";

	private static final Logger LOGGER = LogManager.getLogger(EntitesSQL.class);
	
	public EntitesSQL() {
	}

	public ListeEntitesBean getEntitesByParentId(String niveau, String codeParent, String projection) {
		ListeEntitesBean entites = new ListeEntitesBean();
		PreparedStatement stmt = null;
        ResultSet rs = null;
		
        String sql = "SELECT entite.id_entite as id, entite.code_entite as code, entite.nom_entite as nom, entite.code_parent as parent, entite.xmin as xmin, entite.ymin as ymin,entite.xmax as xmax,entite.ymax as ymax"	
			+ " FROM entites_bouchon as entite"
			+ " WHERE (entite.niveau=? AND entite.code_parent=? AND entite.projection=?);";

    	Connection conn = null;
    	try {
     		conn = EntitesSQL.getConnection(EntitesSQL.database);
 			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, Integer.parseInt(niveau));
			stmt.setString(2, codeParent);
			stmt.setString(3, projection);
			rs = stmt.executeQuery();
            while (rs.next()) {
            	EntiteBean entite = new EntiteBean();
            	entite.setNom(rs.getString("nom"));
            	entite.setCode(rs.getString("code"));
            	entite.setXmin(rs.getFloat("xmin"));
            	entite.setYmin(rs.getFloat("ymin"));
            	entite.setXmax(rs.getFloat("xmax"));
            	entite.setYmax(rs.getFloat("ymax"));

            	entites.addEntites(entite);
            }
			stmt.close();
		} catch (SQLException e) {
			//cleanClose(conn);
			LOGGER.error(e);
		} catch (ClassNotFoundException e) {
			//cleanClose(conn);
			LOGGER.error(e);
		}
		//cleanClose(conn);
		
		return entites;
	}

	private static Connection getConnection(String databaseName) throws ClassNotFoundException,SQLException {
		Connection conn = null;
		String driver = "org.h2.Driver";
		String base = "jdbc:h2:mem:" + databaseName;
		String user = "sa";
		String pwd = "";
		Class.forName(driver);		
		conn = DriverManager.getConnection(base,user,pwd);
		return conn;
	}
	
	/*private static void cleanClose(Connection conn){
		try {
			if (conn != null) {
				Statement st = conn.createStatement();
		        st.execute("SHUTDOWN");
				conn.close();
			}
		} catch (SQLException e) {
			LOGGER.error(e.getLocalizedMessage());
		}
	}*/
}
