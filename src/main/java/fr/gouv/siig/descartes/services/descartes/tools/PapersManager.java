package fr.gouv.siig.descartes.services.descartes.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.Map;

import com.lowagie.text.Rectangle;

/**
 * Class: descartes.tools.PapersManager
 * Classe utilitaire de gestion des formats de papier complémentaires pour l'exportation PDF.
 */
public class PapersManager {

	private static Map<String, String> propertiesMap = null;
	private static Map<String, Rectangle> papersMap = new HashMap<String, Rectangle>();
	
	/**
	 * Staticmethode: initPapers(String)
	 * Initialise la liste des formats disponibles à partir du fichier de définition.
	 * 
	 * Paramètres:
	 * fileName - {String} Nom du fichier de définition
	 * 
	 * Lance:
	 * {Exception} - En cas d'impossibilité de charger le fichier de définition.
	 */
	public static void initPapers(String fileName) throws Exception {
		if (propertiesMap == null) {
			propertiesMap = new HashMap<String, String>();
		}
		try {
			LineNumberReader reader = new LineNumberReader(new FileReader(new File(fileName)));
			String nextLine = reader.readLine();
			while (nextLine != null) {
				if (nextLine.indexOf("=") != -1) {
					propertiesMap.put(nextLine.split("=")[0], nextLine.split("=")[1]);
				}
				nextLine = reader.readLine();
			}
			reader.close();
		} catch (FileNotFoundException e) {
			throw new Exception("Fichier des formats de papier introuvable");
		} catch (IOException e) {
			throw new Exception("Fichier des formats de papier introuvable");
		}
	}

	/**
	 * Staticmethode: getPaper(String)
	 * Retourne un 'papier iText' correspondant à un format complémentaire.
	 * 
	 * Paramètres:
	 * paperName - {String} Nom du format
	 * 
	 * Retour:
	 * {com.lowagie.text.Rectangle} Papier iText
	 * 
	 * Lance:
	 * {Exception} - En cas de format indisponible.
	 */
	public static Rectangle getPaper(String paperName) throws Exception {
		Rectangle paper = papersMap.get(paperName);
		if (paper == null) {
			String w = propertiesMap.get("descartes.paper." + paperName + ".w");
			String h = propertiesMap.get("descartes.paper." + paperName + ".h");
			if (w != null && h != null) {
				paper = new Rectangle(Float.parseFloat(w)*72f/25.4f, Float.parseFloat(h)*72f/25.4f);
				papersMap.put(paperName, paper);
			} else {
				throw new Exception("Format de papier indisponible");
			}
		}
		return paper;
	}
}
