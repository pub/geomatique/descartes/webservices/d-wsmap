package fr.gouv.siig.descartes.services.descartes.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.gouv.siig.descartes.services.descartes.tools.HttpProxy;
import fr.gouv.siig.descartes.services.descartes.tools.HttpSenderException;

/**
 * Class: descartes.services.BasicProxyServlet
 * Service utilitaire correspondant à un proxy applicatif basique pour relayer des requêtes HTTP afin de s'affranchir de la politique de sécurité des navigateurs dite SOP (same origin policy).
 */
@Service
public class BasicProxyService {

	@Value("${app.descartes.global.proxy.host}")
	private String proxyHost;
	@Value("${app.descartes.global.proxy.port}")
	private String proxyPort;
	@Value("${app.descartes.global.proxy.exceptions}")
	private String proxyExceptions;
	@Value("${app.descartes.global.hostMapping}")
	private String hostmapping;
	
	private static final Logger LOGGER = LogManager.getLogger(BasicProxyService.class);
	
    private static final String STRING_CONTENT_LENGTH_HEADER_NAME = "Content-Length";

	private HttpProxy httpProxy;
	private String xmlHostsFileName = null;
        
    private List<String> hopByHopHeaders = new ArrayList<String>();

	public BasicProxyService() {
		LOGGER.info("********** BasicProxyService **********");
	}
    
	/**
	 * Methode: init()
	 * Initialise le service avec les Paramètres du contexte :
	 * 
	 * :
	 * hostmapping - Eventuel fichier de correspondance des Hosts
	 * proxyHost - Host de l'éventuel proxy technique
	 * proxyPort - Port de l'éventuel proxy technique
	 * proxyExceptions - Exceptions de l'éventuel proxy technique
	 * 
	 * (end)
	 */
	@PostConstruct
	public void init() {
		LOGGER.info("*** hostmapping: " + hostmapping);
		if (hostmapping != null && !"".equals(hostmapping)) {
			this.xmlHostsFileName = hostmapping;
		}
		LOGGER.info("*** xmlHostsFileName: " + this.xmlHostsFileName);
		LOGGER.info("*** httpProxyHost: " + proxyHost);
		LOGGER.info("*** httpProxyPort: " + proxyPort);
		LOGGER.info("*** httpProxyExceptions: " + proxyExceptions);
		this.httpProxy = new HttpProxy(proxyHost,proxyPort,proxyExceptions);
        this.initHopByHopHeaders();
        LOGGER.info("********** BasicProxyService OK **********");
	}

   /**
     * Méthode: initHopByHopHeaders
     *
     * Initialise la liste des headers hopByHop
     * http://tools.ietf.org/html/draft-ietf-httpbis-p1-messaging-14#section-7.1.3
     */
    private void initHopByHopHeaders() {
        this.hopByHopHeaders.add("Connection");
        this.hopByHopHeaders.add("Keep-Alive");
        this.hopByHopHeaders.add("Proxy-Authenticate");
        this.hopByHopHeaders.add("TE");
        this.hopByHopHeaders.add("Trailer");
        this.hopByHopHeaders.add("Transfer-Encoding");
        this.hopByHopHeaders.add("Upgrade");
        
        //Correction pour problème quand entêtes sans les majuscules
        this.hopByHopHeaders.replaceAll(String::toUpperCase);
        LOGGER.info("*** initHopByHopHeaders");
    }
        
	/**
	 * Methode: doGet(HttpServletRequest, HttpServletResponse)
     * Relaie une requête GET.
     * 
     * Paramètres de la méthode:
     * request - {javax.servlet.http.HttpServletRequest} Requête HTTP
     * response - {javax.servlet.http.HttpServletResponse} Réponse HTTP
     * 
     * Lance:
     * {IOException} - Si une IOException est déclenchée
     * {ServletException} - Si une ServletException est déclenchée
     */
	public void doGet(Map<String, String> params, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doXXX(params, request, response);
	}

	/**
	 * Methode: doPost(HttpServletRequest, HttpServletResponse)
     * Relaie une requête POST.
     * 
     * Paramètres de la méthode:
     * request - {javax.servlet.http.HttpServletRequest} Requête HTTP
     * response - {javax.servlet.http.HttpServletResponse} Réponse HTTP
     * 
     * Lance:
     * {IOException} - Si une IOException est déclenchée
     * {ServletException} - Si une ServletException est déclenchée
     */
	public void doPost(Map<String, String> body,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doXXX(body, request, response);
	}
	
	private void doXXX(Map<String, String> params, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String urlRequest;
		HttpMethod method = null;
		try {
			urlRequest = this.checkUrlRequest(params,request);
	        LOGGER.debug("***** BasicProxyService doXXX urlRequest: " + urlRequest);
			
			method = this.createMethod(request, urlRequest);

			this.setProxyRequestHeaders(request, method);		
			
			InputStream inputStreamProxyResponse = this.executeRequest(method, response);
		
	        this.setProxyResponseHeaders(response, method);
			
			this.sendResponseContent(inputStreamProxyResponse, response);
		} catch (HostMappingException e) {
			LOGGER.error("***** BasicProxyService doXXX HostMappingException: " + e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
		} catch (HttpSenderException e) {
			LOGGER.error("***** BasicProxyService doXXX HttpSenderException: " + e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
		} finally {
			if (method != null) {
				method.releaseConnection();
			}
		}
	}
	
	private String checkUrlRequest(Map<String, String> params, HttpServletRequest request) throws HostMappingException, HttpSenderException, UnsupportedEncodingException {
		String urlRequest;
		if (params.get("url") != null) {
			urlRequest = URLDecoder.decode(params.get("url"),"UTF-8");
		} else {
			urlRequest = URLDecoder.decode(request.getQueryString(),"UTF-8");
		}
		
		if (urlRequest != null && !"".equals(urlRequest)) {
			urlRequest = HostMapping.mapHostToString(urlRequest, this.xmlHostsFileName);
		} else {
			LOGGER.error("***** BasicProxyService checkUrlRequest urlRequest: Problème");
			throw new HttpSenderException();
		}
		return urlRequest;
	}
	
	private HttpMethod createMethod(HttpServletRequest request, String urlRequest) throws IOException, HttpSenderException {
		HttpMethod method = null;
		if ("POST".equals(request.getMethod())) {
			method = new PostMethod(urlRequest);
			
	        InputStream postBodyStream = request.getInputStream();
	        RequestEntity requestEntity = new ByteArrayRequestEntity(IOUtils.toByteArray(postBodyStream), "application/xml");
	        ((PostMethod) method).setRequestEntity(requestEntity);
		} else if ("GET".equals(request.getMethod())) {
			method = new GetMethod(urlRequest);
		} else {
			LOGGER.error("***** BasicProxyService createMethod method: NON PRIS EN CHARGE");
			throw new HttpSenderException();
		}
		return method;
	}
	
	private InputStream executeRequest(HttpMethod method, HttpServletResponse response) throws HttpException, IOException, HttpSenderException {
		HttpClient client = new HttpClient();
		
		if (hasProxy(method.getURI().getURI() , httpProxy)) {
			LOGGER.debug("***** BasicProxyService executeRequest hasProxy: " + httpProxy.getHost() + " " + httpProxy.getPort());
			client.getHostConfiguration().setProxy(httpProxy.getHost(), httpProxy.getPort());
		}

        int responseStatus = client.executeMethod(method);
	    response.setStatus(responseStatus);
        LOGGER.debug("***** BasicProxyService executeRequest responseStatus: " + responseStatus);
        return method.getResponseBodyAsStream();
	}
	
	private void sendResponseContent(InputStream inputStreamProxyResponse, HttpServletResponse response) throws IOException {
		try {
			OutputStream outputStreamClientResponse = response.getOutputStream();
			IOUtils.copy(inputStreamProxyResponse, outputStreamClientResponse);
		} catch (NullPointerException npe) {
			// nothing
			LOGGER.error("***** BasicProxyService sendResponseContent NullPointerException: " + npe);
		}
	}
	
	private void setProxyRequestHeaders(HttpServletRequest request, HttpMethod method) {
		Enumeration<String> enumerationOfHeaderNames = request.getHeaderNames();
		while(enumerationOfHeaderNames.hasMoreElements()) {
			String stringHeaderName = enumerationOfHeaderNames.nextElement();
			if(stringHeaderName.equalsIgnoreCase(STRING_CONTENT_LENGTH_HEADER_NAME))
				continue;
			Enumeration<String> enumerationOfHeaderValues = request.getHeaders(stringHeaderName);
			while(enumerationOfHeaderValues.hasMoreElements()) {
				String stringHeaderValue = enumerationOfHeaderValues.nextElement();
				Header header = new Header(stringHeaderName, stringHeaderValue);
				method.setRequestHeader(header);
				LOGGER.debug("***** BasicProxyService setProxyRequestHeaders: " + stringHeaderName + " " + stringHeaderValue);
			}
		}
		method.setRequestHeader("Pragma", "no-cache");
		method.setRequestHeader("Cache-Control", "no-cache, max-age=3600, must-revalidate");
    }
	
	private void setProxyResponseHeaders(HttpServletResponse response, HttpMethod method) {
        Header[] responseHeaders = method.getResponseHeaders();
        for (int i=0 ; i<responseHeaders.length ; i++) {
        	Header responseHeader = responseHeaders[i];
        	LOGGER.debug("***** BasicProxyService setProxyResponseHeaders responseHeader : " + responseHeader.getName() + " " + responseHeader.getValue());
        	if(responseHeader.getName().equalsIgnoreCase("Content-Type") && (responseHeader.getValue().equalsIgnoreCase("application/vnd.ogc.wms_xml") || (responseHeader.getValue().equalsIgnoreCase("application/vnd.ogc.se_xml")))){
                responseHeader.setValue("text/xml");
                LOGGER.debug("***** BasicProxyService setProxyResponseHeaders responseHeader changed: " + responseHeader.getName() + " " + responseHeader.getValue());
        	}
        	if (!hopByHopHeaders.contains(responseHeader.getName().toUpperCase())) {
                response.setHeader(responseHeader.getName(), responseHeader.getValue());
                LOGGER.debug("***** BasicProxyService setProxyResponseHeaders response.setHeader: " + responseHeader.getName() + " " + responseHeader.getValue());
            } else {
            	LOGGER.debug("***** BasicProxyService setProxyResponseHeaders NO response.setHeader: " + responseHeader.getName() + " " + responseHeader.getValue());
            }
        }
	}
	
    private static boolean hasProxy(String urlName, HttpProxy httpProxy) throws HttpSenderException {
  	  boolean proxy = false;
  	  if (httpProxy != null && httpProxy.getHost() != null && !httpProxy.getHost().equalsIgnoreCase("") && httpProxy.getPort()!=0) {
  		  proxy = true;
          LOGGER.debug("***** BasicProxyService hasProxy urlName: " + urlName);
  		  try {
  			  if (httpProxy.getUrlExceptions() != null && !httpProxy.getUrlExceptions().equalsIgnoreCase("")) {
  				  LOGGER.debug("***** BasicProxyService hasProxy httpProxy.getUrlExceptions(): " + httpProxy.getUrlExceptions());
  				  URL url = new URL(urlName);				  
  				  String host = url.getHost();
  				  LOGGER.debug("***** BasicProxyService hasProxy host: " + host);
  				  String[] noProxyURLs = httpProxy.getUrlExceptions().split(";");
  				  for (String noProxyURL : noProxyURLs) {
  					  LOGGER.debug("***** BasicProxyService hasProxy noProxyURL: " + noProxyURL);
  					  if (host.startsWith(noProxyURL) || host.endsWith(noProxyURL)) {
  						  proxy = false;
  						  break;
  					  }
  				  }
  			  }
  		  } catch (MalformedURLException e) {
  			  LOGGER.error("***** BasicProxyService hasProxy MalformedURLException: " + urlName, e);
  			  throw new HttpSenderException("URL non valide : " + urlName, e);
  		  }
  	  }
  	  LOGGER.debug("***** BasicProxyService hasProxy Proxy config: " + proxy);
  	  return proxy;
    }
}
