package fr.gouv.siig.descartes.services.descartes.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * Class: descartes.services.CsvExportServlet
 * Servlet utilitaire permettant de proposer l'enregistrement sous forme de fichier d'un contenu CSV généré par le client JavaScript
 * 
 */
@Service
public class CsvExportService{

	private static final Logger LOGGER = LogManager.getLogger(CsvExportService.class);
	
	public CsvExportService() {
		LOGGER.info("********** CsvExportService OK **********");
	}
	/**
	 * Methode: doPost(HttpServletRequest, HttpServletResponse)
	 * Fournit le flux CSV sous forme de fichier enregistrable.
     * 
     * Paramètres de la requête:
     * CSVcontent - {String} Contenu CSV pour le fichier "export.csv" fourni.
     * 
     * Lance:
     * {IOException} - Si une IOException est déclenchée
     * {ServletException} - Si une ServletException est déclenchée
	 * @throws IOException 
	 */
	public void doPost( Map<String, String> body, HttpServletResponse response) throws IOException {
		LOGGER.debug("***** CsvExportService doPost");
		LOGGER.debug("***** CsvExportService CSVcontent: " + body.get("CSVcontent"));
		if (body.get("CSVcontent") != null) {
			this.exportCSV(body.get("CSVcontent"),response);
		}
	}

	private void exportCSV(String csvContent, HttpServletResponse response) throws IOException {
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader ("Content-Disposition", "attachment;	filename=\"export.csv\"");
		response.setContentType("text/plain;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print(csvContent);
		out.flush();
		out.close();
	}
}
