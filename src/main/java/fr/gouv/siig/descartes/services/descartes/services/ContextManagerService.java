package fr.gouv.siig.descartes.services.descartes.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Class: descartes.services.ContextManagerServlet
 * Service proposant la sauvegarde et la restauration de contextes de consultation
 * 
 */
@Service
public class ContextManagerService {

	@Value("${app.descartes.contextManager.contextdir}")
	private String contextmanagerContextdir;
	@Value("${app.descartes.contextManager.maxDays}")
	private String contextmanagerMaxdays;
	
	private static final Logger LOGGER = LogManager.getLogger(ContextManagerService.class);
	
	private String ENCODING = System.getProperty("file.encoding").toUpperCase().equals("CP1252") ? "ISO-8859-1" : System.getProperty("file.encoding");
	protected String contextDir = null;
	private int maxDays = -1;
	
	public ContextManagerService() {
		LOGGER.info("********** ContextManagerService **********");
		LOGGER.info("*** encoding: " + ENCODING);
	}
	
	/**
	 * Methode: init()
	 * Initialise le service avec ses Paramètres :
	 * 
	 * :
	 * contextdir - Répertoire de stockage des fichiers de contexte
	 * maxDays - Eventuelle périodicité en jours de la purge automatique
	 * 
	 */
	@PostConstruct
	public void init() {
		LOGGER.info("*** contextmanagerContextdir: " + contextmanagerContextdir);
		if (contextmanagerContextdir != null && !"".equals(contextmanagerContextdir)) {
			this.contextDir = contextmanagerContextdir;
			if (!this.contextDir.endsWith("/")) {
				this.contextDir += "/";
			}
		}
		LOGGER.info("*** contextmanagerMaxdays: " + contextmanagerMaxdays);
		if (contextmanagerMaxdays != null && !"".equals(contextmanagerMaxdays)) {

			this.maxDays = Integer.parseInt(contextmanagerMaxdays);
		}
		
		LOGGER.info("*** contextDir: " + this.contextDir);
		LOGGER.info("********** ContextManagerService OK **********");
	}
	
	/**
	 * Methode: doGet(HttpServletRequest, HttpServletResponse)
	 * Propose des opérations pour gérer les fichiers sauvegardés.
	 * 
	 * Opération de restauration:
	 * 
     * Le nom du fichier à restaurer est simplement passé en QueryString.
     * 
     * En retour, le service fournit le flux JSON définissant le contexte de visualisation.
     * 
     * Opération de suppression:
     *
     * Le paramètre *delete* identifie le fichier à supprimer.
     * 
     * Aucun retour particulier n'est fournit.
     * 
     * Opération de vérification de l'existence d'un ou plusieurs fichiers:
     * 
     * Le paramètre *check* identifie les fichiers à vérifier, dont les noms sont séparés par des virgules dans la chaine du paramètre.
     * 
     * En retour, le service fournit une chaine (dont le séparateur est |) indiquant une des valeurs suivantes pour chaque fichier testé :
     * 
     * T - si le fichier est disponible et ne porte aucune date d'expiration
     * une date - si le fichier est disponible jusqu'à cette date
     * F - si le fichier n'est plus disponible 
	 */
	public void doGet(Map<String, String> params, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (params.get("delete") != null) {
			String fileName = URLDecoder.decode(params.get("delete"), ENCODING);
			(new File(this.contextDir + fileName)).delete();
			LOGGER.debug("***** ContextManagerService delete: " + fileName);
		} else if (params.get("check") != null) {
			String check = "";
			String fileNames = params.get("check");
			LOGGER.debug("***** ContextManagerService check: " + fileNames);
			if (fileNames.indexOf(",") != -1) {
				String[] files = fileNames.split(",");
				for (int i=0, len=files.length ; i<len ; i++) {
					if (i != 0) {
						check += "|";
					}
					check += this.checkFile(URLDecoder.decode(files[i], ENCODING));
				}
			} else {
				check += this.checkFile(URLDecoder.decode(fileNames, ENCODING));
			}
			response.getWriter().print(new String(check));
		} else if (params.get("get") != null || request.getQueryString() != null) {
			String fileName;
			if (params.get("get") != null) {
				fileName = params.get("get");
			} else {
				fileName = request.getQueryString();
			}
			LOGGER.debug("***** ContextManagerService get: " + fileName);
			fileName = URLDecoder.decode(fileName, ENCODING);
			
			if ( (new File(this.contextDir + fileName)).exists()) {
				File contextFile = new File(this.contextDir + fileName);
				LineNumberReader reader = new LineNumberReader(new FileReader(contextFile));
	
				StringBuffer sb = new StringBuffer();
				if (this.maxDays != -1) {
					sb.append(this.calculateMaxDate(contextFile.lastModified()));
				}
				String nextLine = reader.readLine();
				while (nextLine != null) {
					sb.append(nextLine);
					nextLine = reader.readLine();
				}
				reader.close();
	
				response.setContentType("text/plain");
				response.setCharacterEncoding(ENCODING);
				response.getWriter().print(new String(sb));
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}
		}
	}

	/**
	 * Methode: doPost(HttpServletRequest, HttpServletResponse)
	 * Enregistre un contexte de consultation dans un fichier pour restauration ultérieure.
	 * 
	 * :
	 * La réponse fournie est une chaine représentant un objet JSON identifiant le fichier généré :
	 * :	{"contextFile": "<fichierDeContexte>"}
	 * 
	 * Si le serveur d'application est configuré avec une purge automatique, l'objet JSON comporte une propriété supplémentaire :
	 * :	{"contextFile": "<fichierDeContexte>", "maxDate": "<Date d'expiration>"}
	 * 
     * Paramètres de la requête:
     * view - {String} Nom de la vue personnalisée choisie pour le contexte.
     * context - {String} Contenu du contexte.
	 */
	public void doPost(Map<String, String> body, HttpServletResponse response) throws ServletException, IOException {
		String view = URLDecoder.decode(body.get("view"), ENCODING);
		String context = body.get("context");
		LOGGER.debug("***** ContextManagerServiceview: " + view);
		LOGGER.debug("***** ContextManagerServicecontext: " + context);
		String vide="___";
		if (view.length() <3) {
			view = view + vide.substring(0,vide.length() - view.length());
		}
		LOGGER.debug("***** ContextManagerServiceview: " + view);
		LOGGER.debug("***** ContextManagerServicecontextDir: " + this.contextDir);
		File test = new File(this.contextDir);
		LOGGER.debug("***** ContextManagerService filename: " + test.getName());
		File tempFile = File.createTempFile(view, "", new File(this.contextDir));
		FileOutputStream fos = new FileOutputStream(tempFile);
		fos.write(context.getBytes());
		fos.flush();
		fos.close();
		
		String contextFile = tempFile.getName();
		LOGGER.debug("***** ContextManagerService contextFile: " + contextFile);
		String json = "";
		if (this.maxDays == -1) {
			json = "{\"contextFile\":\"" + contextFile + "\"}";
		} else {
			String expires = this.calculateMaxDate(tempFile.lastModified());
			json = "{\"contextFile\":\"" + contextFile + "\",\"maxDate\":\"" + expires + "\"}";
		}
		LOGGER.debug("***** ContextManagerService json: " + json);
		response.setContentType("text/plain");
		response.setCharacterEncoding(ENCODING);
		response.getWriter().print(json);
		response.getWriter().flush();
		response.getWriter().close();
	}
	
	private String checkFile(String filename) {
		String check = "";
		if ( (new File(this.contextDir + filename)).exists()) {
			if (this.maxDays == -1) {
				check += "T";
			} else {
				File file = new File(this.contextDir + filename);
				check += this.calculateMaxDate(file.lastModified());
			}
		} else {
			check += "F";
		}
		return check;
	}
	
	private String calculateMaxDate(long date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(date);
		calendar.add(Calendar.DAY_OF_YEAR, this.maxDays);
		return DateFormat.getDateInstance().format(calendar.getTime());
	}
}
