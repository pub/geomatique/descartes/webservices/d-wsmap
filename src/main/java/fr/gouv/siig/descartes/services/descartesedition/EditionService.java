package fr.gouv.siig.descartes.services.descartesedition;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.gouv.siig.descartes.services.descartes.tools.HttpProxy;
import fr.gouv.siig.descartes.services.descartes.tools.HttpSender;
import fr.gouv.siig.descartes.services.descartes.tools.HttpSenderException;

@Service
public class EditionService {
	
	@Value("${app.descartes.global.proxy.host}")
	private String proxyHost;
	@Value("${app.descartes.global.proxy.port}")
	private String proxyPort;
	@Value("${app.descartes.global.proxy.exceptions}")
	private String proxyExceptions;
	
	private static final Logger LOGGER = LogManager.getLogger(EditionService.class);
	
	private HttpProxy httpProxy;

	public EditionService() {
		LOGGER.info("********** EditionService **********");
	}
	
	@PostConstruct
	public void init(){
		this.httpProxy = new HttpProxy(proxyHost,proxyPort,proxyExceptions); 
		LOGGER.info("********** EditionService OK **********");
	}
	

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doSomething(req, resp);
    }


	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doSomething(req, resp);
    }

	public void doSomething(HttpServletRequest req, HttpServletResponse resp) throws IOException {

    	
    	StringBuffer buffer = new StringBuffer();
   	  	String line = null;
    	JSONObject jsonFluxWfst;
    	String url;
    	String content;
        String response;
        int status=200;
        
        DocumentBuilderFactory domFactory;
        DocumentBuilder domBuilder;
        Document dom;
        
        String totalInserted="";
        String totalUpdated="";
        String totalDeleted="";
        
        String message="DESCARTES n'est pas chargé de faire la sauvegarde des objets géographiques.</br></br>Ici, pour les tests, la sauvegarde a été faite par du code particulier côté serveur.";
        
        try {
        	
        	 	
        	  BufferedReader reader = req.getReader();
        	  while ((line = reader.readLine()) != null){
        	   	    	buffer.append(line);
        	  }

        	  jsonFluxWfst = (JSONObject) (new JSONParser()).parse(buffer.toString());
        	
        	  url = (String) jsonFluxWfst.get("url");
        	  content = (String) jsonFluxWfst.get("content");
        	  
        	  if(!url.isEmpty() && !content.isEmpty()){
        		  response = HttpSender.getPostResponse(url,new StringBuffer(content),this.httpProxy);

            	  domFactory = DocumentBuilderFactory.newInstance();
    			  domBuilder = domFactory.newDocumentBuilder();        
    			  
    			  if (!response.isEmpty()) {
    					dom = domBuilder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("utf-8"))));

						NodeList nodeList = dom.getElementsByTagName("wfs:totalInserted");
						if (nodeList.item(0) != null && nodeList.item(0).getFirstChild() != null) {
							totalInserted = nodeList.item(0).getFirstChild().getNodeValue();
	    					nodeList = dom.getElementsByTagName("wfs:totalUpdated");
	    					totalUpdated = nodeList.item(0).getFirstChild().getNodeValue();
	    					nodeList = dom.getElementsByTagName("wfs:totalDeleted");
	    					totalDeleted = nodeList.item(0).getFirstChild().getNodeValue();
						} else {
							status=500;
    						message= message + "<br><br> Problème lors de la sauvegarde.<br><br>"+response;
						}
						
    					
    					if(totalInserted == "0" && totalUpdated == "0" && totalDeleted == "0"){
    						status=500;
    						message= message + "<br><br> Problème lors de la sauvegarde.";        
    					}
    					
    			  } else {
    				  status=500;
    				  message= message + "<br><br> Problème lors de la sauvegarde.";
    			  }
        	  } else {
        		  status=500;
				  message= message + "<br><br> Problème lors de la sauvegarde.";
        	  }
        	  
			  
        } catch (ParseException e) {
        	status=500;
        	message= message + "<br><br> Problème lors de la sauvegarde.";
 		}  catch (HttpSenderException e) {
 			status=500;
 			message= message + "<br><br> Problème lors de la sauvegarde.";
 		} catch (ParserConfigurationException e) {
 			status=500;
 			message= message + "<br><br> Problème lors de la sauvegarde.";
		} catch (SAXException e) {
			status=500;
			message= message + "<br><br> Problème lors de la sauvegarde.";
		} 	  

        String jsonReturned = "{\"status\":" + status + ",\"message\": \"" + message + "\"}";
        
        resp.setContentType("application/json");
        resp.setStatus(status);
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(new String(jsonReturned));
    }

}

  
