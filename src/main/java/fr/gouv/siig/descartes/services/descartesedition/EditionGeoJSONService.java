package fr.gouv.siig.descartes.services.descartesedition;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EditionGeoJSONService {

	@Value("${app.descartes.editionGeoJSON.geojsondir}")
	private String editionGeoJSONgeojsondir;
	
	private static final Logger LOGGER = LogManager.getLogger(EditionGeoJSONService.class);
	
	protected String geojsonDir = null;
	
	public EditionGeoJSONService() {
		LOGGER.info("********** EditionGeoJSONService **********");
	}
	
	@PostConstruct
	public void init() {
		LOGGER.info("***** editionGeoJSONgeojsondir: " + editionGeoJSONgeojsondir);
		if (editionGeoJSONgeojsondir != null && !editionGeoJSONgeojsondir.equals("")) {
			this.geojsonDir =  editionGeoJSONgeojsondir;
			if (!this.geojsonDir.endsWith("/")) {
				this.geojsonDir += "/";
			}
		}
		LOGGER.info("***** geojsonDir: " + this.geojsonDir);
		LOGGER.info("********** EditionGeoJSONService OK **********");
	}

	public void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doSomething(request, response);
	}

	public void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doSomething(request, response);
	}

	public void doSomething(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		StringBuffer buffer = new StringBuffer();
		String line = null;
		JSONObject jsonFlux = new JSONObject();
		JSONObject jsonFile = new JSONObject();
		String fileUrl;
		int status = 200;

		String message = "DESCARTES n'est pas chargé de faire la sauvegarde des objets géographiques.</br></br>Ici, pour les tests, la sauvegarde a été faite par du code particulier côté serveur.";
		
		try {

			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}

			jsonFlux = (JSONObject) (new JSONParser()).parse(buffer.toString());
			
			
			fileUrl = jsonFlux.get("url").toString();
			fileUrl = this.geojsonDir+fileUrl;
			LOGGER.debug("***** EditionGeoJSON fileUrl: " + fileUrl);
			
			byte[] encoded = Files.readAllBytes(Paths.get(fileUrl));

			jsonFile = (JSONObject) (new JSONParser()).parse(new String(encoded, Charset.defaultCharset()));
			LOGGER.debug("***** EditionGeoJSON jsonFile: " + jsonFile);
			JSONObject contentObject =  (JSONObject) jsonFlux.get("content");
			
			JSONObject addFlux = (JSONObject) (new JSONParser()).parse(contentObject.get("addObjects").toString());
			JSONObject updatedFlux = (JSONObject) (new JSONParser()).parse(contentObject.get("updatedObjects").toString());
			JSONObject removedFlux = (JSONObject) (new JSONParser()).parse(contentObject.get("removedObjects").toString());

			JSONArray addArray = (JSONArray) addFlux.get("features");
			JSONArray updatedArray = (JSONArray) updatedFlux.get("features");
			JSONArray removedArray = (JSONArray) removedFlux.get("features");
			JSONArray arrayFile = (JSONArray) jsonFile.get("features");

			arrayFile = this.removeFeatures(arrayFile, removedArray);
			arrayFile = this.removeFeatures(arrayFile, updatedArray);
			arrayFile = this.addFeatures(arrayFile, updatedArray, false);
			arrayFile = this.addFeatures(arrayFile, addArray, false);

			byte data[] = jsonFile.toString().getBytes();
			LOGGER.debug("***** EditionGeoJSON fileUrl: " + fileUrl);
			Path file = Paths.get(fileUrl);
			LOGGER.debug("***** EditionGeoJSON file: " + file);
			Files.write(file, data);

		} catch (Exception e) {
			status = 500;
			message = message + "<br><br> Problème lors de la sauvegarde.";
			LOGGER.error("***** EditionGeoJSON Problème lors de la sauvegarde.");
		}

		String jsonReturned = "{\"status\":" + status + ",\"message\": \"" + message + "\"}";
		LOGGER.debug("***** EditionGeoJSON jsonReturned: " + jsonReturned);
		resp.setContentType("application/json");
		resp.setStatus(status);
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().print(new String(jsonReturned));

	}
	
	/**
	 * 
	 * @param init
	 * @param toRemove
	 * @return
	 */
	private JSONArray removeFeatures(JSONArray init, JSONArray toRemove){
		JSONArray tmp = init;
		
		int i = tmp.size() - 1;
		while (i >= 0) {

			JSONObject tmpObj = (JSONObject) tmp.get(i);

			String id2 = null;

			if (tmpObj.containsKey("id")) {
				id2 = tmpObj.get("id").toString();

			}

			for (int j = 0; j < toRemove.size(); j++) {

				if (((JSONObject) toRemove.get(j)).containsKey("id")
						&& id2 != null) {

					String id1 = ((JSONObject) toRemove.get(j)).get(
							"id").toString();

					if (id1.equals(id2)) {
						tmp.remove(i);
					}
				}
			}
			i--;
		}
		
		return tmp;
	}
	
	/**
	 * 
	 * @param init
	 * @param toRemove
	 * @return
	 */
	private JSONArray addFeatures(JSONArray init, JSONArray toAdd, boolean newId){
		JSONArray tmp = init;
		
		for (int j = 0; j < toAdd.size(); j++) {

			JSONObject tmpObj = (JSONObject) toAdd.get(j);
			
			if(newId){
				tmpObj.put("id",String.valueOf((int) (Math.random() * 1000000)));
			}
			tmp.add(tmpObj);
		}
		
		return tmp;
	}

}
