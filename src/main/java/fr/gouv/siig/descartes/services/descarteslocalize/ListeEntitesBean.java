package fr.gouv.siig.descartes.services.descarteslocalize;

import java.util.ArrayList;

public class ListeEntitesBean {

		private EntiteBean entite_parent = new EntiteBean();
		private ArrayList<EntiteBean> entites = new ArrayList<EntiteBean>();
		
		public ListeEntitesBean() {
		}
		
		public void setEntite_parent(EntiteBean entite_parent) {
			this.entite_parent = entite_parent;
		}

		public EntiteBean getEntite_parent() {
			return entite_parent;
		}

		public void addEntites(EntiteBean entite) {
			this.entites.add(entite);
		}

		public ArrayList<EntiteBean> getEntites() {
			return entites;
		}
		
		public void setEntites(ArrayList<EntiteBean> entites) {
			this.entites = entites;
		}
		
		public String getJSON() {
			StringBuffer json = new StringBuffer(); 
			json.append("[\n");
			int index = 0;
			for (EntiteBean entiteBean : this.entites) {
				json.append(entiteBean.getJSON());
				if (index != (this.entites.size() -1)) {
					json.append(",");
				}
				index ++;
				json.append("\n");
			}
			json.append("]\n");
			
			return json.toString();
		}
		
}
