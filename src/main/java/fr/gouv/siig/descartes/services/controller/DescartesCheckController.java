package fr.gouv.siig.descartes.services.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Controller
public class DescartesCheckController {

	private static final Logger LOGGER = LogManager.getLogger(DescartesCheckController.class);
	
	@Value("${app.descartes.global.proxy.host}")
	private String proxyHost;
	@Value("${app.descartes.global.proxy.port}")
	private String proxyPort;
	@Value("${app.descartes.global.proxy.exceptions}")
	private String proxyExceptions;
	@Value("${app.descartes.global.hostMapping}")
	private String hostmapping;

	@Value("${app.descartes.contextManager.contextdir}")
	private String contextmanagerContextdir;
	@Value("${app.descartes.contextManager.maxDays}")
	private String contextmanagerMaxdays;
	
	@Value("${app.descartes.exportPDF.extraPapers}")
	private String exportPdfExtraPapers;
	@Value("${app.descartes.exportPDF.repertoireLogos}")
	private String exportPdfRepertoireLogos;
	@Value("${app.descartes.exportPDF.tailleMaxLogo}")
	private String exportPdfTailleMaxLogo;
	
	@Value("${app.descartes.getFeature.stylesCSS}")
	private String getfeatureStylescss;
	@Value("${app.descartes.getFeature.XSLfile}")
	private String getfeatureXslfile;
	@Value("${app.descartes.getFeature.httpConnectionTimeout}")
	private int getfeatureHttpconnectiontimeout;
	@Value("${app.descartes.getFeature.httpSocketTimeout}")
	private int getfeatureHttpsockettimeout;
	@Value("${app.descartes.getFeature.imageLocateItem}")
	private String getfeatureImagelocateitem;
	@Value("${app.descartes.getFeature.imageLocateMenu}")
	private String getfeatureImagelocatemenu;
	
	@Value("${app.descartes.getFeatureInfo.httpConnectionTimeout}")
	private int getfeatureinfoHttpconnectiontimeout;
	@Value("${app.descartes.getFeatureInfo.httpSocketTimeout}")
	private int getfeatureinfoHttpsockettimeout;
	@Value("${app.descartes.getFeatureInfo.stylesCSS}")
	private String getfeatureinfoStylescss;
	@Value("${app.descartes.getFeatureInfo.XSLfile}")
	private String getfeatureinfoXslfile;
	
	
	@CrossOrigin
    @GetMapping(value="/checkconfig")
    @ResponseBody
    public String test() throws Exception {
		LOGGER.debug("*****DEBUT");
		String outputContent = "";
		
		LOGGER.debug("proxyHost: " + proxyHost);
		LOGGER.debug("proxyPort: " + proxyPort);
		LOGGER.debug("proxyExceptions: " + proxyExceptions);
		LOGGER.debug("hostmapping: " + hostmapping);
		
		LOGGER.debug("contextmanagerContextdir: " + contextmanagerContextdir);
		LOGGER.debug("contextmanagerMaxdays: " + contextmanagerMaxdays);
		
		LOGGER.debug("exportPdfExtraPapers: " + exportPdfExtraPapers);
		LOGGER.debug("exportPdfRepertoireLogos: " + exportPdfRepertoireLogos);
		LOGGER.debug("exportPdfTailleMaxLogo: " + exportPdfTailleMaxLogo);
		
		LOGGER.debug("getfeatureStylescss: " + getfeatureStylescss);
		LOGGER.debug("getfeatureXslfile: " + getfeatureXslfile);
		LOGGER.debug("getfeatureHttpconnectiontimeout: " + getfeatureHttpconnectiontimeout);
		LOGGER.debug("getfeatureHttpsockettimeout: " + getfeatureHttpsockettimeout);
		LOGGER.debug("getfeatureImagelocateitem: " + getfeatureImagelocateitem);
		LOGGER.debug("getfeatureImagelocatemenu: " + getfeatureImagelocatemenu);
		
		LOGGER.debug("getfeatureinfoHttpconnectiontimeout: " + getfeatureinfoHttpconnectiontimeout);
		LOGGER.debug("getfeatureinfoHttpsockettimeout: " + getfeatureinfoHttpsockettimeout);
		LOGGER.debug("getfeatureinfoStylescss: " + getfeatureinfoStylescss);
		LOGGER.debug("getfeatureinfoXslfile: " + getfeatureinfoXslfile);
		
		
		outputContent += "proxyHost: " + proxyHost + "</br>";
		outputContent += "proxyPort: " + proxyPort + "</br>";
		outputContent += "proxyExceptions: " + proxyExceptions + "</br>";
		outputContent += "hostmapping: " + hostmapping + "</br>";
		
		outputContent += "contextmanagerContextdir: " + contextmanagerContextdir + "</br>";
		outputContent += "contextmanagerMaxdays: " + contextmanagerMaxdays + "</br>";
		
		outputContent += "exportPdfExtraPapers: " + exportPdfExtraPapers + "</br>";
		outputContent += "exportPdfRepertoireLogos: " + exportPdfRepertoireLogos + "</br>";
		outputContent += "exportPdfTailleMaxLogo: " + exportPdfTailleMaxLogo + "</br>";
		
		outputContent += "getfeatureStylescss: " + getfeatureStylescss + "</br>";
		outputContent += "getfeatureXslfile: " + getfeatureXslfile + "</br>";
		outputContent += "getfeatureHttpconnectiontimeout: " + getfeatureHttpconnectiontimeout + "</br>";
		outputContent += "getfeatureHttpsockettimeout: " + getfeatureHttpsockettimeout + "</br>";
		outputContent += "getfeatureImagelocateitem: " + getfeatureImagelocateitem + "</br>";
		outputContent += "getfeatureImagelocatemenu: " + getfeatureImagelocatemenu + "</br>";
		
		outputContent += "getfeatureinfoHttpconnectiontimeout: " + getfeatureinfoHttpconnectiontimeout + "</br>";
		outputContent += "getfeatureinfoHttpsockettimeout: " + getfeatureinfoHttpsockettimeout + "</br>";
		outputContent += "getfeatureinfoStylescss: " + getfeatureinfoStylescss + "</br>";
		outputContent += "getfeatureinfoXslfile: " + getfeatureinfoXslfile + "</br>";
			
		LOGGER.debug("*****FIN");
		
		return outputContent;
	}
	
	@CrossOrigin
    @GetMapping("/version")
    public String version() {
    		return "redirect:/manage/info";
    }
    
	@CrossOrigin
    @GetMapping("/info")
    public String info() {
    		return "redirect:/manage/info";
    }
    
	@CrossOrigin
    @GetMapping("/metrics")
    public String metrics() {
    		return "redirect:/manage/metrics";
    }
    
	@CrossOrigin
    @GetMapping("/health")
    public String health() {
    		return "redirect:/manage/health";
    }
    
	@CrossOrigin
    @GetMapping("/prometheus")
    public String prometheus() {
    		return "redirect:/manage/prometheus";
    }
}
