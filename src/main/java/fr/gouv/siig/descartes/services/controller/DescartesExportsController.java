package fr.gouv.siig.descartes.services.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.CrossOrigin;

import fr.gouv.siig.descartes.services.descartes.services.CsvExportService;
import fr.gouv.siig.descartes.services.descartes.services.PdfExportMapService;
import fr.gouv.siig.descartes.services.descartes.services.PngExportMapService;

@Controller
public class DescartesExportsController {
	
	private static final Logger LOGGER = LogManager.getLogger(DescartesExportsController.class);
	
	@Autowired
	public CsvExportService csvExportService;
	
	@Autowired
	public PngExportMapService pngExportMapService;
	
	@Autowired
	public PdfExportMapService pdfExportMapService;
	
	@CrossOrigin
    @PostMapping(value="/api/v1/csvExport")
	public void csvexport(@RequestParam Map<String, String> body, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller csvexport");
		LOGGER.debug("CSVcontent: " + body.get("CSVcontent"));
		this.csvExportService.doPost(body, response);		
		LOGGER.debug("***** FIN controller csvexport");

	}

	@CrossOrigin
    @PostMapping(value="/api/v1/exportPNG")
	public void exportPNG(@RequestParam Map<String, String> body, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller exportPNG POST");
		LOGGER.debug("paramsExport: " + body.get("paramsExport"));
		this.pngExportMapService.doPost(body, request, response);		
		LOGGER.debug("***** FIN controller exportPNG POST");

	}

	@CrossOrigin
    @GetMapping(value="/api/v1/exportPNG")
	public void exportPNGProgression(HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller exportPNG GET");
		this.pngExportMapService.doGet(request, response);
		LOGGER.debug("***** FIN controller exportPNG GET");
	}
	
	@CrossOrigin
    @PostMapping(value="/api/v1/exportPDF")
	public void exportPDF(@RequestParam Map<String, String> body, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller exportPDF");
		LOGGER.debug("paramsExport: " + body.get("paramsExport"));
		LOGGER.debug("printerSetupParams: " + body.get("printerSetupParams"));
		this.pdfExportMapService.doPost(body, request, response);	
		LOGGER.debug("***** FIN controller exportPDF");

	}
	
	@CrossOrigin
    @GetMapping(value="/api/v1/exportPDF")
	public void exportPDFProgression(HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller exportPDF GET");
		this.pdfExportMapService.doGet(request, response);
		LOGGER.debug("***** FIN controller exportPDF GET");
	}
}
