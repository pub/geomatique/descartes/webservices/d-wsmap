package fr.gouv.siig.descartes.services.descarteslocalize;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ListeService {

	
	private static final Logger LOGGER = LogManager.getLogger(ListeService.class);
	
	public ListeService() {
		LOGGER.info("********** ListeService **********");
	}

	@PostConstruct
	public void init() {
        LOGGER.info("********** ListeService OK **********");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		this.doGet(request, response);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String niveau = null;
		if (request.getParameter("niveau") != null) {
			niveau = request.getParameter("niveau");
		}
		
		String codeParent = null;
		if (request.getParameter("parent") != null) {
			codeParent = request.getParameter("parent");
		}else{
			codeParent = "0";
		}
		
		String projection = null;
		if (request.getParameter("projection") != null) {
			projection = request.getParameter("projection");
		}
		
		String outputContent = "";
		
		EntitesSQL entitesSQL = new EntitesSQL();
		ListeEntitesBean listeEntiteBean = entitesSQL.getEntitesByParentId(niveau, codeParent, projection);
	
		if (listeEntiteBean.getEntites().size() != 0) {
			outputContent = listeEntiteBean.getJSON();
		}

		response.getWriter().print(outputContent);
	}

}
