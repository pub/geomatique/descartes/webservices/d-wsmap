package fr.gouv.siig.descartes.services.descartes.services;

import fr.gouv.siig.descartes.services.descartes.tools.HttpProxy;
import fr.gouv.siig.descartes.services.descartes.tools.XmlTools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xpath.XPathAPI;

/**
 * Class: descartes.services.ExportMapServlet
 * Servlet *abstrait* de génération d'exports PDF ou PNG.
 * 
 * :
 * Le servlet lance un thread dérivant de <descartes.services.AbstractMapWriter> dont l'avancement est examiné pour informer l'appelant.
 * 
 * 
 * Classes dérivées:
 * - <descartes.services.PdfExportMapService>
 * - <descartes.services.PngExportMapService>
 */
@Service
public abstract class ExportMapService {

	private static final Logger LOGGER = LogManager.getLogger(ExportMapService.class);
	
	protected static final String repertoireLogos = "repertoireLogos";
	protected static final String tailleMaxLogo = "tailleMaxLogo";
	protected static final String EXPORT_PNG = "PNG";
	protected static final String EXPORT_PDF = "PDF";
	protected static final String CONTENT_TYPE_PNG = "image/png";
	protected static final String CONTENT_TYPE_PDF = "application/pdf";

	private String xmlHostsFileName = null;
	private HttpProxy httpProxy;
	protected String logoDirectory;
	protected String logoSizeMax;
	protected String exportFileFormat;
	protected String contentType;
	
	private int logoSizeMaxDefault = 64;
	
	public ExportMapService() {
		LOGGER.info("********** ExportMapService **********");
	}

	/**
	 * Methode: init()
	 * Initialise le servlet avec les Paramètres du contexte :
	 * 
	 * :
	 * hostmapping - Eventuel fichier de correspondance des Hosts
	 * proxyHost - Host de l'éventuel proxy
	 * proxyPort - Port de l'éventuel proxy
	 * proxyExceptions - Exceptions de l'éventuel proxy
	 * 
	 */
	public void init(String httpProxyHost, String httpProxyPort, String httpProxyExceptions, String hostmapping){
		LOGGER.info("*** hostmapping: " + hostmapping);
		if (hostmapping != null && !"".equals(hostmapping)) {
			this.xmlHostsFileName = hostmapping;
		}
		LOGGER.info("*** xmlHostsFileName: " + this.xmlHostsFileName);
		LOGGER.info("*** httpProxyHost: " + httpProxyHost);
		LOGGER.info("*** httpProxyPort: " + httpProxyPort);
		LOGGER.info("*** httpProxyExceptions: " + httpProxyExceptions);
		this.httpProxy = new HttpProxy(httpProxyHost,httpProxyPort,httpProxyExceptions);
		LOGGER.info("********** ExportMapService OK **********");
	}

	/**
	 * Methode: doGet(HttpServletRequest, HttpServletResponse)
     * Informe l'appelant de l'avancement du thread (Pourcentage, accès au téléchargement du fichier, Page d'erreur).
     * 
     * Paramètres de la requête:
     * mapWriter - {<AbstractMapWriter>} Thread de génération en cours de traitement.
     * 
     * 
     * Lance:
     * {IOException} - Si une IOException est déclenchée
     * {ServletException} - Si une ServletException est déclenchée
     */
    public void doGet (HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
        HttpSession session = request.getSession(true);
        Object oMapWriter = session.getAttribute("mapWriter");
        AbstractMapWriter mapWriter = null;
        if (this.exportFileFormat.equals(ExportMapService.EXPORT_PDF)) {
			mapWriter = (PdfMapWriter)oMapWriter;
		}
		if (this.exportFileFormat.equals(ExportMapService.EXPORT_PNG)) {
			mapWriter = (PngMapWriter)oMapWriter;
		}
        progression(mapWriter,response);
    }

    //@SuppressWarnings("null")
	/**
	 * Methode: doPost(HttpServletRequest, HttpServletResponse)
     * Traite la requête en fonction de la présence ou non du paramètre "paramsExport".
     * - Si le paramètre "paramsExport" est nul, il s'agit du téléchargement du fichier généré.
     * - Dans le cas contraire, il initialise le thread de génération avec le paramètre "paramsExport" et éventuellement avec le paramètre optionnel "printerSetupParams".
     * 
     * Paramètres de la requête:
     * paramsExport - {String} Flux XML du contenu de l'export souhaité, conforme au schéma fourni par la méthode <SchemaFactory.getExport>.
     * printerSetupParams - {String} Flux XML des Paramètres de mise en page de l'export souhaité, conforme au schéma fourni par la méthode <SchemaFactory.getPrinterParams>.
     * 
     * Paramètres de la méthode:
     * request - {javax.servlet.http.HttpServletRequest} Requête HTTP
     * response - {javax.servlet.http.HttpServletResponse} Réponse HTTP
     * 
     * Lance:
     * {IOException} - Si une IOException est déclenchée
     * {ServletException} - Si une ServletException est déclenchée
     */
	public void doPost (Map<String, String> body, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    	HttpSession session = request.getSession();
        if (body.get("paramsExport")!=null) {
        	try {
            	String paramsExport = body.get("paramsExport").replaceAll("&","&amp;");
            	
    			Document paramsExternes = XmlTools.readAndValidateXmlString(paramsExport, SchemaFactory.getExport());
            	
            	String[] elementNames = {"Url","UrlLegende","UrlLogo"};
				Document paramsExportDOM = HostMapping.mapHostsToDoc(paramsExternes,this.xmlHostsFileName,elementNames);
				AbstractMapWriter mapWriter = null;
				if (this.exportFileFormat.equals(ExportMapService.EXPORT_PDF)) {
					mapWriter = new PdfMapWriter(this.httpProxy);
				}
				if (this.exportFileFormat.equals(ExportMapService.EXPORT_PNG)) {
					mapWriter = new PngMapWriter(this.httpProxy);
				}
				
				String titreCarte = this.getStringParam(paramsExportDOM, "//Titre");
				mapWriter.setTitreCarte( (titreCarte == null) ? "null" : titreCarte);
				mapWriter.setLargeurPixCarte(this.getIntParam(paramsExportDOM, "//Largeur"));
				mapWriter.setHauteurPixCarte(this.getIntParam(paramsExportDOM, "//Hauteur"));
				mapWriter.setAuteurCarte(this.getStringParam(paramsExportDOM, "//Auteur"));
				mapWriter.setCopyrightCarte(this.getStringParam(paramsExportDOM, "//Copyright"));
				mapWriter.setDescriptionCarte(this.getStringParam(paramsExportDOM, "//Description"));
				mapWriter.setDateValiditeCarte(this.getStringParam(paramsExportDOM, "//DateValidite"));
				mapWriter.setApplicationInfos(this.getStringParam(paramsExportDOM, "//ApplicationInfos"));
				mapWriter.setCreator(this.getStringParam(paramsExportDOM, "//Creator"));
				mapWriter.setUrlCartes(this.getUrlArrayParam(paramsExportDOM, "//Couche/Url"));
				mapWriter.setUrlLegendes(this.getUrlArrayParam(paramsExportDOM, "//UrlLegende"));
				mapWriter.setUrlLogos(this.getUrlArrayParam(paramsExportDOM, "//UrlLogo"));
				mapWriter.setOpacites(this.getFloatArrayParam(paramsExportDOM, "//Couche/Opacite"));
				mapWriter.setFileLogos(this.getStringArrayParam(paramsExportDOM, "//FichierLogo", this.logoDirectory));
				mapWriter.setSizeMaxLogo((this.logoSizeMax == null) ? this.logoSizeMaxDefault : Integer.parseInt(this.logoSizeMax));

				if (body.get("printerSetupParams") != null) {
					try {
						Document printerSetupParams = XmlTools.readAndValidateXmlString(request.getParameter("printerSetupParams"), SchemaFactory.getPrinterParams());
						mapWriter.setPageFormat(this.getStringParam(printerSetupParams, "//Format"));
						mapWriter.setPageOrientation(this.getStringParam(printerSetupParams, "//Orientation"));
						mapWriter.setPageUnits(this.getStringParam(printerSetupParams, "//Margins/Units"));
						mapWriter.setPageMarginTop(this.getIntParam(printerSetupParams, "//Margins/Top"));
						mapWriter.setPageMarginBottom(this.getIntParam(printerSetupParams, "//Margins/Bottom"));
						mapWriter.setPageMarginLeft(this.getIntParam(printerSetupParams, "//Margins/Left"));
						mapWriter.setPageMarginRight(this.getIntParam(printerSetupParams, "//Margins/Right"));
					} catch (ParserConfigurationException e) {
						// tant pis : valeurs par defaut
					} catch (SAXException e) {
						// tant pis : valeurs par defaut
					}
				}

	        	session.setAttribute("mapWriter", mapWriter);
	        	Thread t = new Thread(mapWriter);
	        	t.start();
	        	progression(mapWriter,response);

			} catch (Exception e) {
				isError(response.getOutputStream(), e);
			}
        } else if (session.getAttribute("mapWriter")!=null){
	        try {

	        	AbstractMapWriter mapWriter = null;
	    		if (this.exportFileFormat.equals(ExportMapService.EXPORT_PDF)) {
	    			mapWriter = (PdfMapWriter) session.getAttribute("mapWriter");
	    		}
	    		if (this.exportFileFormat.equals(ExportMapService.EXPORT_PNG)) {
	    			mapWriter = (PngMapWriter) session.getAttribute("mapWriter");
	    		}
	        	session.removeAttribute("mapWriter");
				ByteArrayOutputStream baos = mapWriter.getWriter();
				response.setHeader("Expires", "0");
				response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
				response.setHeader("Pragma", "public");
				response.setHeader ("Content-Disposition", "attachment;	filename=\"" + mapWriter.getTitreCarte() + "." + this.exportFileFormat.toLowerCase() + "\"");
				response.setContentType(this.contentType);
				response.setContentLength(baos.size());
				ServletOutputStream out = response.getOutputStream();
				baos.writeTo(out);
				out.flush();
				session.setAttribute("mapWriter",null);
	        }
	        catch(Exception e) {
	        	isError(response.getOutputStream(), e);
	        }
        } else {
        	isError(response.getOutputStream(), new Exception("La session a expir&eacute;e"));
        }
    }
	
	private String getStringParam(Document node, String elementName) throws DOMException, TransformerException, UnsupportedEncodingException {
		String param = null;
		try {
			String paramTemp = XPathAPI.selectSingleNode(node,elementName).getFirstChild().getNodeValue();
	        param = new String (paramTemp.getBytes("UTF-8"),"UTF-8" );
		} catch (NullPointerException e) {
			// tant pis !
		}
		return param;
	}
	
	private int getIntParam(Document node, String elementName) throws DOMException, TransformerException {
		int param = 0;
		try {
			String paramTemp = XPathAPI.selectSingleNode(node,elementName).getFirstChild().getNodeValue();
	        param = Integer.parseInt(paramTemp);
		} catch (NullPointerException e) {
			// tant pis !
		}
		return param;
	}
	
	private String[] getUrlArrayParam(Document node, String elementName) throws DOMException, TransformerException {
		NodeList nodeList = XPathAPI.selectNodeList(node,elementName);
		String[] params = new String[nodeList.getLength()];
		if (nodeList.getLength() != 0) {
			String param;
			for (int i=0 ; i < nodeList.getLength() ; i++) {
				StringWriter stringWriter = new StringWriter();
				OutputFormat outputFormat = new OutputFormat("XML","UTF-8",false);
				outputFormat.setOmitXMLDeclaration(true);
				XMLSerializer serializer = new XMLSerializer(stringWriter, outputFormat);
				try {
					serializer.serialize((Element) nodeList.item(i));
					stringWriter.flush();
					stringWriter.close();
				} catch (IOException e) {
					// tant pis !
				}
				param = stringWriter.toString();
				param = param.substring(param.indexOf(">")+1);
				param = param.substring(0, param.lastIndexOf("<"));
				
				params[i] = param.replaceAll("&amp;","&").replace("+", "%2B");
			}
		}
		return params;
	}
	
	private String[] getStringArrayParam(Document node, String elementName, String outputPrefix) throws DOMException, TransformerException {
		if (outputPrefix == null) {
			outputPrefix = "";
		}
		NodeList nodeList = XPathAPI.selectNodeList(node,elementName);
		String[] params = new String[nodeList.getLength()];
		if (nodeList.getLength() != 0) {
			String param;
			for (int i=0 ; i < nodeList.getLength() ; i++) {
				param = nodeList.item(i).getFirstChild().getNodeValue();
				params[i] = outputPrefix + param.replaceAll("&amp;","&").replace("+", "%2B");
			}
		}
		return params;
	}
	
	private float[] getFloatArrayParam(Document node, String elementName) throws DOMException, TransformerException {
		NodeList nodeList = XPathAPI.selectNodeList(node,elementName);
		float[] params = new float[nodeList.getLength()];
		if (nodeList.getLength() != 0) {
			String param;
			for (int i=0 ; i < nodeList.getLength() ; i++) {
				param = nodeList.item(i).getFirstChild().getNodeValue();
				params[i] = Float.parseFloat(param)/100;
			}
		}
		return params;
	}
	
	//@SuppressWarnings("null")
	private void progression(AbstractMapWriter mapWriter, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        if (mapWriter == null) {
        	isError(response.getOutputStream(),new Exception("Pas de parametre 'pdf' en requete."));
        } else {
	        switch (mapWriter.getAvancement()) {
	        	case -1:
	        		isError(response.getOutputStream(),mapWriter.getErreur());
	        		return;
	        	case 100:
	        		isFinished(response.getOutputStream());
	        		return;
	        	default:
	        		isBusy(mapWriter, response.getOutputStream());
	        		return;
	        }
        }
	}
    
	
	private void isBusy(AbstractMapWriter mapWriter, ServletOutputStream stream) throws IOException {
		stream.print("<html><head><title>Merci de patienter...</title><meta http-equiv=\"Refresh\" content=\"2\"></head><body bgcolor=\"#FFFFFF\" text=\"#0000A0\">");
		stream.print("<div align=\"center\" style=\"font-family:Arial;font-size:8pt;\"><br/>");
		stream.print(progressBar(mapWriter.getAvancement()) + "<br/>");
		stream.print(String.valueOf(mapWriter.getAvancement()));
		stream.print("% du document est pr&ecirc;t.<br>Merci de patienter, la page va se rafraichir automatiquement (toutes les 2 secondes)</div></body>\n</html>");
	}

	private void isFinished(ServletOutputStream stream) throws IOException {
		try {
			stream.print("<html><head><title>La carte est pr&ecirc;te!</title></head><body bgcolor=\"#FFFFFF\" text=\"#0000A0\">");
			stream.print("<div align=\"center\" style=\"font-family:Arial;font-size:8pt;\"><br/>");
			stream.print("<form method=\"POST\" onsubmit=\"this.style.display='none';document.getElementById('fin').style.display='block'\">Le document est pr&ecirc;t:<br/><br/><input type=\"Submit\" style=\"font-family:Arial;font-size:8pt;\" value=\"T&eacute;l&eacute;charger le " + this.exportFileFormat + "\"></form>");
			stream.print("<div id=\"fin\" style=\"display:none\"><br/>T&eacute;l&eacute;chargement termin&eacute;.<br/><br/><a href=\"\" onclick=\"window.close()\">Fermer cette fen&ecirc;tre.</a></div>");
			stream.print("</div></body></html>");
		} catch (Exception ex) {
			isError(stream, ex);
		}
	}

	private void isError(ServletOutputStream stream, Exception ex) throws IOException {
		stream.print("<html><head><title>Erreur</title></head><body>");
		stream.print("Une erreur est survenue. "+ex.toString() + "</body></html>");
	}
	
	private String progressBar(int pourcent) {
		int barWidth = 400;
		int barDone = pourcent * barWidth / 100;
		int barToDo = barWidth - barDone;
		String colorDone = "#0000A0";
		String colorToDo = "#FFFFFF";
		StringBuffer barHTML = new StringBuffer();
		barHTML.append("<table cellpadding=\"0\" cellspacing=\"0\" width=\"" + barWidth + "\" height=\"8\" style=\";border:1px solid " + colorDone + ";\"><tr>");
		if (pourcent != 0) {
			barHTML.append("<td style=\"background-color:" + colorDone + ";\" width=\"" + barDone + "\">&nbsp;</td>");
		}
		barHTML.append("<td style=\"background-color:" + colorToDo + ";\" width=\"" + barToDo + "\">&nbsp;</td>");
		barHTML.append("</tr></table>");

		return barHTML.toString();
	}
	
}

