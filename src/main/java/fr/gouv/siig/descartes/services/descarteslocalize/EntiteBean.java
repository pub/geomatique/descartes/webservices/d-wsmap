package fr.gouv.siig.descartes.services.descarteslocalize;

public class EntiteBean {

	private String code = "0";
	private String nom;
	private float xmin;
	private float xmax;
	private float ymin;
	private float ymax;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getXmin() {
		return xmin;
	}
	public void setXmin(float xmin) {
		this.xmin = xmin;
	}
	public float getXmax() {
		return xmax;
	}
	public void setXmax(float xmax) {
		this.xmax = xmax;
	}
	public float getYmin() {
		return ymin;
	}
	public void setYmin(float ymin) {
		this.ymin = ymin;
	}
	public float getYmax() {
		return ymax;
	}
	public void setYmax(float ymax) {
		this.ymax = ymax;
	}
	
	public String getJSON() {
		StringBuffer buf = new StringBuffer();
		
		buf.append("{");
		buf.append("\"code\":\"" + this.code +"\",");
		buf.append("\"nom\":\"" + this.nom +"\",");
		buf.append("\"xmin\":" + Float.toString(this.xmin) +",");
		buf.append("\"ymin\":" + Float.toString(this.ymin) +",");
		buf.append("\"xmax\":" + Float.toString(this.xmax) +",");
		buf.append("\"ymax\":" + Float.toString(this.ymax));
		
		buf.append("}");
		return buf.toString();
	}

}
