package fr.gouv.siig.descartes.services.descartes.services;

/**
 * Class: descartes.services.SchemaFactory
 * Fournisseur de schémas XSD pour validation de flux XML. 
 */
public class SchemaFactory {
	
	private SchemaFactory() {
		// magic
	}
	
	/**
	 * Staticmethode: getRequest()
	 * Retourne le schéma pour les interrogations WMS et WFS.
	 * 
	 * Retour:
	 * {String} Schéma XSD correspondant :
	 * (start code)
	 * 	<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	 * 
	 * 	<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">
	 * 		<xs:element name=\"Infos\">
	 * 			<xs:complexType>
	 * 				<xs:sequence>
	 * 					<xs:element name=\"Layer\" maxOccurs=\"unbounded\">
	 * 						<xs:complexType>
	 * 							<xs:sequence>
	 * 								<xs:element name=\"Name\">
	 * 									<xs:simpleType>
	 * 										<xs:restriction base=\"xs:string\">
	 * 											<xs:minLength value=\"1\"/>
	 * 										</xs:restriction>
	 * 									</xs:simpleType>
	 * 								</xs:element>
	 * 								<xs:element name=\"Request\" maxOccurs=\"unbounded\">
	 * 									<xs:simpleType>
	 * 										<xs:restriction base=\"xs:string\">
	 * 											<xs:minLength value=\"1\"/>
	 * 										</xs:restriction>
	 * 									</xs:simpleType>
	 * 								</xs:element>
	 * 								<xs:element name=\"Version\" type=\"xs:string\" minOccurs=\"0\"/>
	 * 								<xs:element name=\"FeatureGeometryName\" type=\"xs:string\" minOccurs=\"0\"/>
	 * 								<xs:element name=\"LayerMinScale\" type=\"xs:float\" minOccurs=\"0\"/>
	 * 								<xs:element name=\"LayerMaxScale\" type=\"xs:float\" minOccurs=\"0\"/>
	 * 							</xs:sequence>
	 * 						</xs:complexType>
	 * 					</xs:element>
	 * 				</xs:sequence>
	 * 			</xs:complexType>
	 * 		</xs:element>
	 * 	</xs:schema>
	 * (end)
	 */
	public static String getRequest() {
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">");
		sb.append("<xs:element name=\"Infos\"><xs:complexType><xs:sequence>");
		sb.append("<xs:element name=\"Layer\" maxOccurs=\"unbounded\"><xs:complexType><xs:sequence>");
		sb.append("<xs:element name=\"Name\"><xs:simpleType><xs:restriction base=\"xs:string\"><xs:minLength value=\"1\"/></xs:restriction></xs:simpleType></xs:element>");
		sb.append("<xs:element name=\"Request\" maxOccurs=\"unbounded\"><xs:simpleType><xs:restriction base=\"xs:string\"><xs:minLength value=\"1\"/></xs:restriction></xs:simpleType></xs:element>");
		sb.append("<xs:element name=\"Version\" type=\"xs:string\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"FeatureGeometryName\" type=\"xs:string\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"LayerMinScale\" type=\"xs:float\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"LayerMaxScale\" type=\"xs:float\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"LayerId\" type=\"xs:string\" minOccurs=\"0\"/>");
		sb.append("</xs:sequence></xs:complexType></xs:element>");
		sb.append("</xs:sequence></xs:complexType></xs:element>");
		sb.append("</xs:schema>");
		
		return sb.toString();
	}

	/**
	 * Staticmethode: getExport()
	 * Retourne le schéma pour les exportations PDF et PNG.
	 * 
	 * Retour:
	 * {String} Schéma XSD correspondant :
	 * (start code)
	 * 	<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	 * 
	 * 	<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">
	 * 		<xs:element name=\"Export\">
	 * 			<xs:complexType>
	 * 				<xs:sequence>
	 * 					<xs:element name=\"Titre\"/>
	 * 					<xs:element name=\"Largeur\"/>
	 * 					<xs:element name=\"Hauteur\"/>
	 * 					<xs:element name=\"Couche\" maxOccurs=\"unbounded\">
	 * 						<xs:complexType>
	 * 							<xs:sequence>
	 * 								<xs:element name=\"Url\"/>
	 * 								<xs:element name=\"Opacite\"/>
	 * 							</xs:sequence>
	 * 						</xs:complexType>
	 * 					</xs:element>
	 * 					<xs:element name=\"UrlLegende\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>
	 * 					<xs:element name=\"UrlLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>
	 * 					<xs:element name=\"FichierLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>
	 * 					<xs:element name=\"Auteur\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"Copyright\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"Description\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"DateValidite\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"ApplicationInfos\" minOccurs=\"0\"/>
	 * 					<xs:element name=\"Creator\" minOccurs=\"0\"/>
	 * 				</xs:sequence>
	 * 			</xs:complexType>
	 * 		</xs:element>
	 * 	</xs:schema>
	 * (end)
	 */
	public static String getExport() {
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">");
		sb.append("<xs:element name=\"Export\"><xs:complexType><xs:sequence>");
		sb.append("<xs:element name=\"Titre\"/>");
		sb.append("<xs:element name=\"Largeur\"/>");
		sb.append("<xs:element name=\"Hauteur\"/>");
		sb.append("<xs:element name=\"Couche\" maxOccurs=\"unbounded\"><xs:complexType><xs:sequence>");
		sb.append("<xs:element name=\"Url\"/>");
		sb.append("<xs:element name=\"Opacite\"/>");
		sb.append("</xs:sequence></xs:complexType></xs:element>");
		sb.append("<xs:element name=\"UrlLegende\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>");
		sb.append("<xs:element name=\"UrlLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>");
		sb.append("<xs:element name=\"FichierLogo\" minOccurs=\"0\" maxOccurs=\"unbounded\"/>");
		sb.append("<xs:element name=\"Auteur\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"Copyright\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"Description\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"DateValidite\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"ApplicationInfos\" minOccurs=\"0\"/>");
		sb.append("<xs:element name=\"Creator\" minOccurs=\"0\"/>");
		sb.append("</xs:sequence></xs:complexType></xs:element>");
		sb.append("</xs:schema>");
		return sb.toString();
	}
	
	/**
	 * Staticmethode: getPrinterParams()
	 * Retourne le schéma pour les parametres d'impression PDF.
	 * 
	 * Retour:
	 * {String} Schéma XSD correspondant :
	 * (start code)
	 * 	<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	 * 
	 * 	<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">
	 * 		<xs:element name=\"PrinterSetup\">
	 * 			<xs:complexType>
	 * 				<xs:sequence>
	 * 					<xs:element name=\"Format\"/>
	 * 					<xs:element name=\"Orientation\"/>
	 * 					<xs:element name=\"Margins\">
	 * 						<xs:complexType>
	 * 							<xs:sequence>
	 * 								<xs:element name=\"Units\"/>
	 * 								<xs:element name=\"Top\"/>
	 * 								<xs:element name=\"Bottom\"/>
	 * 								<xs:element name=\"Left\"/>
	 * 								<xs:element name=\"Right\"/>
	 * 							</xs:sequence>
	 * 						</xs:complexType>
	 * 					</xs:element>
	 * 				</xs:sequence>
	 * 			</xs:complexType>
	 * 		</xs:element>
	 * </xs:schema>
	 * (end)
	 */
	public static String getPrinterParams() {
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">");
		sb.append("<xs:element name=\"PrinterSetup\"><xs:complexType><xs:sequence>");
		sb.append("<xs:element name=\"Format\"/>");
		sb.append("<xs:element name=\"Orientation\"/>");
		sb.append("<xs:element name=\"Margins\"><xs:complexType><xs:sequence>");
		sb.append("<xs:element name=\"Units\"/>");
		sb.append("<xs:element name=\"Top\"/>");
		sb.append("<xs:element name=\"Bottom\"/>");
		sb.append("<xs:element name=\"Left\"/>");
		sb.append("<xs:element name=\"Right\"/>");
		sb.append("</xs:sequence></xs:complexType></xs:element>");
		sb.append("</xs:sequence></xs:complexType></xs:element>");
		sb.append("</xs:schema>");
		return sb.toString();
	}
	
}

