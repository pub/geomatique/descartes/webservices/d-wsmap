package fr.gouv.siig.descartes.services.descartes.tools.imagery;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;

/**
 * Class: descartes.tools.imagery.GetImageHelper
 * Servlet utilitaire de fourniture d'images à partir d'images internes à la bibliothèque.
 * 
 * :
 * Les images sont stockées prés de la classe du servlet.
 * 
 * :
 * Le servlet est nécessaire pour lors de l'utilisation du servlet <descartes.services.GetFeatureServlet> utilisant au moins un des Paramètres optionnels 'imageLocateMenu' et 'imageLocateItem'.
 * 
 * Hérite de:
 * - javax.servlet.http.HttpServlet
 * 
 * Exemple de configuration du servlet dans le descripteur de déploiement:
 * (start code)
 * 	<servlet>
 * 		<servlet-name>getImage</servlet-name>
 * 		<servlet-class>fr.gouv.siig.descartes.services.descartes.tools.imagery.GetImageHelper</servlet-class>
 * 	</servlet>
 */
public class GetImageHelper {

	private static final Logger LOGGER = LogManager.getLogger(GetImageHelper.class);
	
	public GetImageHelper() {
		LOGGER.info("********** GetImageHelper OK **********");
	}

	/**
	 * Methode: doGet(HttpServletRequest, HttpServletResponse)
	 * Fournit l'image souhaitée dans le flux de la réponse.
     * 
     * Paramètres de la requête:
     * img - {String} Nom du fichier de l'image
     * 
     * Paramètres de la méthode:
     * request - {javax.servlet.http.HttpServletRequest} Requête HTTP
     * response - {javax.servlet.http.HttpServletResponse} Réponse HTTP
     * 
     * Lance:
     * {IOException} - Si une IOException est déclenchée
     * {ServletException} - Si une ServletException est déclenchée
	 */
	public void doGet(Map<String, String> params, HttpServletResponse response) throws ServletException, IOException {
        String image = "resources/images/" + params.get("img");
        LOGGER.debug("*****GetImageHelper image: " + image);
        String type = params.get("img").split("\\.")[1];
        LOGGER.debug("*****GetImageHelper type: " + type);
        response.setHeader("content-type","image/"+type);
        ServletOutputStream out = response.getOutputStream();
        try {
            byte [] tmpByte = new byte[200];
            InputStream in = new ClassPathResource(image, this.getClass().getClassLoader()).getInputStream();         
            @SuppressWarnings("unused")
			int tm;
            while ((tm=in.read(tmpByte))!=-1)
              {
              out.write(tmpByte);
              }
        } catch (Exception e) {
        	LOGGER.error("***** Impossible de charger l'image : " + e);
            throw new IOException("Impossible de charger l'image : " + e);
        }
	}
}
