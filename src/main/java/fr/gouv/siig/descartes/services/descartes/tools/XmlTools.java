package fr.gouv.siig.descartes.services.descartes.tools;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class: descartes.tools.XmlTools
 * Classe utilitaire de traitements XML.
 */
public class XmlTools {

	private static final Logger LOGGER = LogManager.getLogger(XmlTools.class);
	
	private static TransformerFactory tFactory = null;
	private static ValidatorErrorHandler validatorErrorHandler = new ValidatorErrorHandler();
	
	private XmlTools() {
		// magic
	}
	
	/**
	 * Retourne la fabrique de transformations XSL-T.
	 * @return	Fabrique de transformations XSL-T
	 */
	private static TransformerFactory getTranformerInstance() {
		if (tFactory == null) {
			tFactory = TransformerFactory.newInstance();
		}
		return tFactory;
	}

	/**
	 * Staticmethode: executeXSL(String, String, Hashtable<String,String>)
	 * Exécution d'une transformation XSL-T à partir d'un fichier.
	 * 
	 * Paramètres:
	 * XMLflux - {String} Flux XML source
	 * XSLstylesheet - {String} Nom du fichier XSL-T à utiliser 
	 * paramsKVP - {Hashtable<String,String>} Key-Name des Paramètres de la transformation
	 * 
	 * Retour:
	 * {String} Flux XML résultat
	 * 
	 * Lance:
	 * {TransformerException} - Si une TransformerException est déclenchée
	 */
	public static String executeXSL(String XMLflux, String XSLstylesheet, Hashtable<String,String> paramsKVP) throws TransformerException {
        StringWriter fluxXML = new StringWriter();
        StreamSource xmlSource = new StreamSource( new StringReader(XMLflux) );
        TransformerFactory theFactory = XmlTools.getTranformerInstance();
        Transformer xslTransformer = null;

        xslTransformer = theFactory.newTransformer(new StreamSource(XSLstylesheet));
        if (paramsKVP != null) {
        	Set<Map.Entry<String,String>> KVPs = paramsKVP.entrySet();
        	Iterator <Map.Entry<String,String>> paramsIt = KVPs.iterator();
        	while (paramsIt.hasNext()) {
        		Map.Entry<String,String> KVP = paramsIt.next();
        		String K = KVP.getKey();
        		String V = KVP.getValue();
            	xslTransformer.setParameter(K, V);
            }
        }
        xslTransformer.transform(xmlSource,new StreamResult(fluxXML));

        return fluxXML.toString();
    }
	
	/**
	 * Staticmethode: executeInternalXSL(String, InputStream, Hashtable<String,String>)
	 * Exécution d'une transformation XSL-T à partir d'un flux.
	 * 
	 * Paramètres:
	 * XMLflux - {String} Flux XML source
	 * XSLstylesheet - {InputStream} Ressource contenant la transformation XSL-T à utiliser
	 * paramsKVP - {Hashtable<String,String>} Key-Name des Paramètres de la transformation
	 * 
	 * Retour:
	 * {String} Flux XML résultat
	 * 
	 * Lance:
	 * {TransformerException} - Si une TransformerException est déclenchée
	 */
	public static String executeInternalXSL(String XMLflux, InputStream XSLstylesheet, Hashtable<String,String> paramsKVP) throws TransformerException {
        StringWriter fluxXML = new StringWriter();
        StreamSource xmlSource = new StreamSource( new StringReader(XMLflux) );
        TransformerFactory theFactory = XmlTools.getTranformerInstance();
        Transformer xslTransformer = null;

        xslTransformer = theFactory.newTransformer(new StreamSource(XSLstylesheet));
        if (paramsKVP != null) {
        	Set<Map.Entry<String,String>> KVPs = paramsKVP.entrySet();
        	Iterator <Map.Entry<String,String>> paramsIt = KVPs.iterator();
        	while (paramsIt.hasNext()) {
        		Map.Entry<String,String> KVP = paramsIt.next();
        		String K = KVP.getKey();
        		String V = KVP.getValue();
            	xslTransformer.setParameter(K, V);
            }
        }
        xslTransformer.transform(xmlSource,new StreamResult(fluxXML));

        return fluxXML.toString();
    }
	
	/**
	 * Staticmethode: getInnerEncoding(String)
	 * Retourne l'encodage interne d'un flux XML.
	 * 
	 * Paramètres:
	 * fluxXML - {String} Flux XML à valider
	 * 
	 * Retour:
	 * {String} Encodage interne du flux
	 * 
	 * Lance:
	 * {ParserConfigurationException} - Si une ParserConfigurationException est déclenchée
	 * {SAXException} - Si une SAXException est déclenchée
	 * {IOException} - Si une IOException est déclenchée
	 */
	public static String getInnerEncoding(String fluxXML) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(fluxXML)));
		
		return document.getXmlEncoding();
	}
	
	/**
	 * Staticmethode: readAndValidateXmlString(String, String)
	 * Retourne un arbre DOM à partir d'un flux XML validé selon un schéma XSD.
	 * 
	 * Paramètres:
	 * fluxXML - {String} Flux XML à valider
	 * xsdFile - {String} Nom du fichier décrivant le schéma XSD
	 * 
	 * Retour:
	 * {Document} Arbre DOM validé
	 * 
	 * Lance:
	 * {ParserConfigurationException} - Si une ParserConfigurationException est déclenchée
	 * {SAXException} - Si une SAXException est déclenchée
	 * {IOException} - Si une IOException est déclenchée
	 */
	public static Document readAndValidateXmlString(String fluxXML, String xsdFile) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(fluxXML)));
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Source schemaFile = new StreamSource(new StringReader(xsdFile));
	    Schema schema = factory.newSchema(schemaFile);
	    Validator validator = schema.newValidator();

	    XmlTools.validatorErrorHandler.clear();
	    validator.setErrorHandler(XmlTools.validatorErrorHandler);
	    DOMSource source = new DOMSource(document);
	    validator.validate(source);
	    if (XmlTools.validatorErrorHandler.getErrors().size() != 0) {
	    	String msg = "";
	    	for (String error : XmlTools.validatorErrorHandler.getErrors()) {
	    		msg = error + "\n";
	    	}
	    	LOGGER.debug("DESCARTES XmlTools readAndValidateXmlString validatorErrorHandler message" + msg);
	    	throw new SAXException(msg);
	    }
	    return document;
	}

	/**
	 * Staticmethode: readAndValidateXmlFile(File, String)
	 * Retourne un arbre DOM à partir d'un fichier XML validé selon un schéma XSD.
	 * 
	 * Paramètres:
	 * fileXML - {File} Fichier XML à valider
	 * xsdFile - {String} Nom du fichier décrivant le schéma XSD
	 * 
	 * Retour:
	 * {Document} Arbre DOM validé
	 * 
	 * Lance:
	 * {ParserConfigurationException} - Si une ParserConfigurationException est déclenchée
	 * {SAXException} - Si une SAXException est déclenchée
	 * {IOException} - Si une IOException est déclenchée
	 */
	public static Document readAndValidateXmlFile(File fileXML, String xsdFile) throws IOException, ParserConfigurationException, SAXException {
		StringBuffer sb = new StringBuffer();
		LineNumberReader reader = new LineNumberReader(new FileReader(fileXML));
		String line = reader.readLine();
		while (line != null) {
			sb.append(line);
			line = reader.readLine();
		}
		reader.close();
		return readAndValidateXmlString(sb.toString(), xsdFile);
	}
	
	public static class ValidatorErrorHandler implements ErrorHandler {
		private List<String> errors = new ArrayList<String>();
		
		public void warning(SAXParseException ex) {
	    	errors.add(ex.getMessage());
	    }

	    public void error(SAXParseException ex) {
	    	errors.add(ex.getMessage());
	    }

	    public void fatalError(SAXParseException ex) {
	    	errors.add(ex.getMessage());
	    }
	    
	    public List<String> getErrors() {
	    	return this.errors;
	    }
	    
	    public void clear() {
	    	this.errors = new ArrayList<String>();
	    }
	}

}
