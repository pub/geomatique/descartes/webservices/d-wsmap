package fr.gouv.siig.descartes.services.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import fr.gouv.siig.descartes.services.descartes.services.BasicProxyService;

@Controller
public class DescartesProxyController {
	
	private static final Logger LOGGER = LogManager.getLogger(DescartesProxyController.class);
	
	@Autowired
	public BasicProxyService basicProxyService;
	
	@CrossOrigin
    @PostMapping(value="/api/v1/proxy")
	public void proxypost(@RequestParam Map<String, String> body, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller Proxy POST");
		basicProxyService.doPost(body, request, response);	
		LOGGER.debug("***** FIN controller Proxy POST");

	}
	
	@CrossOrigin
    @GetMapping(value="/api/v1/proxy")
	public void proxyget(@RequestParam Map<String, String> params, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller Proxy GET");
		basicProxyService.doGet(params, request, response);
		LOGGER.debug("***** FIN controller Proxy GET");
	}

}
