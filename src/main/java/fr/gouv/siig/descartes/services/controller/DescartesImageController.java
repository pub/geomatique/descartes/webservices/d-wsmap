package fr.gouv.siig.descartes.services.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.CrossOrigin;

import fr.gouv.siig.descartes.services.descartes.tools.imagery.GetImageHelper;

@Controller
public class DescartesImageController {
	
	private static final Logger LOGGER = LogManager.getLogger(DescartesImageController.class);
	
	public GetImageHelper getImageHelper = new GetImageHelper();
	
	@CrossOrigin
    @GetMapping(value="/api/v1/getImage")
	public void getimage(@RequestParam Map<String, String> params, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller getImage GET");
		this.getImageHelper.doGet(params, response);
		LOGGER.debug("***** FIN controller getImage GET");
	}

}
