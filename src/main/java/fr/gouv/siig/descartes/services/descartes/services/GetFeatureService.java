package fr.gouv.siig.descartes.services.descartes.services;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Class: descartes.services.GetFeatureServlet
 * Servlet d'accès aux données sémantiques de serveurs WFS.
 * 
 * Hérite de:
 * - <descartes.services.OwsHelperService>
 */
@Service
public class GetFeatureService extends OwsHelperService {	
	
	@Value("${app.descartes.global.proxy.host}")
	private String proxyHost;
	@Value("${app.descartes.global.proxy.port}")
	private String proxyPort;
	@Value("${app.descartes.global.proxy.exceptions}")
	private String proxyExceptions;
	@Value("${app.descartes.global.hostMapping}")
	private String hostmapping;
	
	@Value("${app.descartes.getFeature.stylesCSS}")
	private String getfeatureStylescss;
	@Value("${app.descartes.getFeature.XSLfile}")
	private String getfeatureXslfile;
	@Value("${app.descartes.getFeature.httpConnectionTimeout}")
	private int getfeatureHttpconnectiontimeout;
	@Value("${app.descartes.getFeature.httpSocketTimeout}")
	private int getfeatureHttpsockettimeout;
	@Value("${app.descartes.getFeature.imageLocateItem}")
	private String getfeatureImagelocateitem;
	@Value("${app.descartes.getFeature.imageLocateMenu}")
	private String getfeatureImagelocatemenu;	
	
	private static final Logger LOGGER = LogManager.getLogger(GetFeatureService.class);
	
	private static final int DEFAULT_HTTP_CONNECTION_TIMEOUT = 10000; //ms
	private static final int DEFAULT_HTTP_SOCKET_TIMEOUT = 10000; //ms
	
	public GetFeatureService() {
		LOGGER.info("********** GetFeatureService **********");
	}

	/**
	 * Methode: init()
	 * Initialise le service avec les Paramètres :
	 * 
	 * :
	 * stylesCSS - Eventuel fichier de la feuille de style CSS présentant la réponse en flux HTML, remplaçant la feuille de style CSS par défaut 
	 * XSLfile - Eventuel fichier de transformation XSL-T formattant la réponse en flux HTML, remplaçant la transformation par défaut
	 * imageLocateMenu - Eventuel pictogramme, remplaçant le pictogramme par défaut pour l'entéte de la colonne "Localiser"
	 * imageLocateItem - Eventuel pictogramme, remplaçant le pictogramme par défaut pour l'accès à la localisation d'un objet
	 * 
	 */
	@PostConstruct
	public void init() {
		this.internalXslFile = "resources/getXmlFeature.xsl";
		LOGGER.info("***** internalXslFile: " + this.internalXslFile);
		LOGGER.info("***** getfeatureStylescss: " + getfeatureStylescss);
		if (getfeatureStylescss != null && !getfeatureStylescss.equals("")) {
			this.stylesCssFileName = getfeatureStylescss;
		}
		LOGGER.info("***** stylesCssFileName: " + this.stylesCssFileName);
		LOGGER.info("***** getfeatureXslfile: " + getfeatureXslfile);
		if (getfeatureXslfile != null && !getfeatureXslfile.equals("")) {
			this.xsl2HtmlFile = getfeatureXslfile;
		}
		LOGGER.info("***** xsl2HtmlFile: " + this.xsl2HtmlFile);
		LOGGER.info("***** getfeatureImagelocatemenu: " + getfeatureImagelocatemenu);
		if (getfeatureImagelocatemenu != null && !getfeatureImagelocatemenu.equals("")) {
			this.imageLocateMenu = getfeatureImagelocatemenu;
		}
		LOGGER.info("***** imageLocateMenu: " + this.imageLocateMenu);
		LOGGER.info("***** getfeatureImagelocateitem: " + getfeatureImagelocateitem);
		if (getfeatureImagelocateitem != null && !getfeatureImagelocateitem.equals("")) {
			this.imageLocateItem = getfeatureImagelocateitem;
		}
		LOGGER.info("***** imageLocateItem: " + this.imageLocateItem);
		this.httpConnectionTimeout = DEFAULT_HTTP_CONNECTION_TIMEOUT;
		if (getfeatureHttpconnectiontimeout != 0) {
			this.httpConnectionTimeout = getfeatureHttpconnectiontimeout;
		}
		LOGGER.info("***** httpConnectionTimeout: " + this.httpConnectionTimeout);
		this.httpSocketTimeout = DEFAULT_HTTP_SOCKET_TIMEOUT;
		if (getfeatureHttpsockettimeout != 0) {
			this.httpSocketTimeout = getfeatureHttpsockettimeout;
		}
		LOGGER.info("***** httpSocketTimeout: " + this.httpSocketTimeout);

		super.init(proxyHost, proxyPort, proxyExceptions, hostmapping);
		LOGGER.info("********** GetFeatureService OK **********");
	}

}
