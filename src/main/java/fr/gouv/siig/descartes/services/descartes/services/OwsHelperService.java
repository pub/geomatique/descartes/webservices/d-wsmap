package fr.gouv.siig.descartes.services.descartes.services;

import org.apache.xpath.XPathAPI;

import fr.gouv.siig.descartes.services.descartes.tools.HttpProxy;
import fr.gouv.siig.descartes.services.descartes.tools.HttpSender;
import fr.gouv.siig.descartes.services.descartes.tools.HttpSenderException;
import fr.gouv.siig.descartes.services.descartes.tools.XmlTools;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.concurrent.CompletableFuture;
import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;

import org.apache.commons.httpclient.Header;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class: descartes.services.OwsHelperService
 * Servlet *abstrait* d'accès aux données sémantiques de serveurs WMS ou WFS.
 * 
 * 
 * Classes dérivées:
 * - <descartes.services.GetFeatureInfoService>
 * - <descartes.services.GetFeatureService>
 */
@Service
public class OwsHelperService {

	private static final Logger LOGGER = LogManager.getLogger(OwsHelperService.class);
	
	private static final String DEFAULT_CONTENT_TYPE = "text/xml";
	private static final String INTERNAL_STYLE_CSS = "resources/styles.css";
	protected static final String STYLES_CSS_KEY = "stylesCSS";
	protected static final String XSL_FILE_KEY = "XSLfile";
	protected static final String IMAGE_LOCATE_MENU_KEY = "imageLocateMenu";
	protected static final String IMAGE_LOCATE_ITEM_KEY = "imageLocateItem";
	protected static final String HTTP_CONNECTION_TIMEOUT = "httpConnectionTimeout";
	protected static final String HTTP_SOCKET_TIMEOUT = "httpSocketTimeout";
	private static final int DEFAULT_HTTP_CONNECTION_TIMEOUT = 10000; //ms
	private static final int DEFAULT_HTTP_SOCKET_TIMEOUT = 10000; //ms
	
	/**
	 * Nom du fichier XSLT personnalise pour la conversion des reponses WMS / WFS en flux HTML
	 */
	protected String xsl2HtmlFile;
	
	/**
	 * Nom du fichier XSLT embarque pour la conversion des reponses WMS / WFS en flux HTML
	 */
	protected String internalXsl2HtmlFile = "resources/toHtmlResult.xsl";
	
	/**
	 * Nom du fichier XSLT embarque pour la conversion des reponses WMS / WFS en flux JSON
	 */
	protected String internalXsl2JsonFile = "resources/toJsonResult.xsl";
	
	/**
	 * Nom du fichier XSLT embarque pour l'unification des reponses WMS / WFS
	 */
	protected String internalXslFile;
	
	protected String stylesCssFileName;
	private String xmlHostsFileName = null;
	private HttpProxy httpProxy;
	protected String outputFormat = DEFAULT_CONTENT_TYPE;
	protected String imageLocateMenu;
	protected String imageLocateItem;
	protected int httpConnectionTimeout = DEFAULT_HTTP_CONNECTION_TIMEOUT;
	protected int httpSocketTimeout = DEFAULT_HTTP_SOCKET_TIMEOUT;

	private List<Header> requestHeaders = new ArrayList<Header>();
	
	public OwsHelperService() {
		LOGGER.info("********** OwsHelperService **********");
	}
	
	public void init(String httpProxyHost, String httpProxyPort, String httpProxyExceptions, String hostmapping){
		LOGGER.info("*** hostmapping: " + hostmapping);
		if (hostmapping != null && !"".equals(hostmapping)) {
			this.xmlHostsFileName = hostmapping;
		}
		LOGGER.info("*** xmlHostsFileName: " + this.xmlHostsFileName);
		LOGGER.info("*** httpProxyHost: " + httpProxyHost);
		LOGGER.info("*** httpProxyPort: " + httpProxyPort);
		LOGGER.info("*** httpProxyExceptions: " + httpProxyExceptions);
		this.httpProxy = new HttpProxy(httpProxyHost,httpProxyPort,httpProxyExceptions); 
		LOGGER.info("********** OwsHelperService OK **********");
	}


	/**
	 * Methode: doPost(HttpServletRequest, HttpServletResponse)
	 * Traite la requête d'accès aux données sémantiques de serveurs WMS ou WFS.
     * 
     * Paramètres de la requête:
     * infos - {String} Flux XML listant les ressources WMS ou WFS à contacter, conforme au schéma fourni par la méthode <SchemaFactory.getRequest>.
     * format - {"JSON" | "HTML"} Format souhaité pour le flux de la réponse. Optionnel ("XML" par défaut)
     * dataMask - {String} Flux FEI pour filtrer les données interrogées selon un ensemble de conditions attributaires. Utilisé pour les requêtes attributaires
     * gmlMask - {String} Flux GML pour filtrer les données interrogées selon l'inclusion dans un objet polygonal. Utilisé pour les sélections polygonales
     * pixelMask - {String} Coordonnées "pixels" correspondant à un point de la carte. Utilisé pour les sélections ponctuelles et les info-bulles
     * withReturn - {"true" | "false"} Indique si le flux de la réponse doit inclure les informations nécessaires à la localisation directe sur les objets fournis. Optionnel pour les formats JSON et HTML ("false" par défaut)
     * withCsvExport - {"true" | "false"} Indique si le flux de la réponse doit inclure les informations nécessaires à l'exportation CSV des objets fournis. Optionnel pour les formats JSON et HTML ("false" par défaut)
     * 
     * Exemple de valeur pour le paramètre "dataMask":
     * (start code)
     * 	<PropertyIsEqualTo>
     * 		<PropertyName>Vitesse_limite</PropertyName>
     * 		<Literal>110 km/h</Literal>
     * 	</PropertyIsEqualTo>
     * (end)
     * 
     * Exemple de valeur pour le paramètre "gmlMask":
     * (start code)
     * 	<wfs:FeatureCollection xmlns:wfs=\"http://www.opengis.net/wfs\">
     * 		<gml:featureMember xmlns:gml=\"http://www.opengis.net/gml\">
     * 			<feature:features xmlns:feature=\"http://mapserver.gis.umn.edu/mapserver\">
     * 				<feature:geometry>
     * 					<gml:Polygon>
     * 						<gml:outerBoundaryIs>
     * 							<gml:LinearRing>
     * 								<gml:coordinates decimal=\".\" cs=\",\" ts=\" \">791779.9552533333,1854000.6277333333 ...</gml:coordinates>
     * 							</gml:LinearRing>
     * 						</gml:outerBoundaryIs>
     * 					</gml:Polygon>
     * 				</feature:geometry>
     * 			</feature:features>
     * 		</gml:featureMember>
     * 	</wfs:FeatureCollection>
     * (end)
     * 
     * Exemple de valeur pour le paramètre "pixelMask":
     * (start code)
     * 	#X=154#Y=96
     * (end)
     * 
     * Paramètres de la méthode:
     * 
     * Lance:
     * {IOException} - Si une IOException est déclenchée
     * {ServletException} - Si une ServletException est déclenchée
     */
	public String doPost(Map<String, String> body, Map<String,String> headers)  throws Exception {
		//
		LOGGER.debug("***** OwsHelperService doPost");
		String message = "ERREUR";
		Iterator<Entry<String, String>> iterator = headers.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<String, String> mapentry  = iterator.next();
			Header header = new Header(mapentry.getKey(), mapentry.getValue());
			this.requestHeaders.add(header);
		}
		
		String timeout = body.get("timeout");
		if (timeout != null) {
			this.httpConnectionTimeout = Integer.parseInt(timeout);
            this.httpSocketTimeout = Integer.parseInt(timeout);
            LOGGER.debug("***** OwsHelperService doPost TIMEOUT httpConnectionTimeout: " + this.httpConnectionTimeout + " ms");
			LOGGER.debug("***** OwsHelperService doPost TIMEOUT httpSocketTimeout: " + this.httpSocketTimeout + " ms");
		}
		
		String resultFormat = body.get("format");
		if (resultFormat != null) {
			if (resultFormat.equals("JSON")) {
				this.outputFormat = "text/plain";
			} else if (resultFormat.equals("HTML")) {
				this.outputFormat = "text/html";
			}
		}
		
		String layerName = null;
		String layerRequest = null;
		String layerVersion = "";
		String layerFeatureGeometryName = "msGeometry";
		NodeList layerRequestNodes = null;
		NodeList layerVersionNodes = null;
		NodeList layerFeatureGeometryNameNodes = null;
		Node layerNameNode = null;
		
		try {
			String theGetFeatureXxxURL = body.get("infos").replaceAll("%3C", "\\&lt;");

			
			theGetFeatureXxxURL = URLDecoder.decode(theGetFeatureXxxURL,"UTF-8").replaceAll("&","&amp;");

			Document infosExternes = XmlTools.readAndValidateXmlString(theGetFeatureXxxURL, SchemaFactory.getRequest());

			String[] elementNames = {"Request"};
			Document infosdoc = HostMapping.mapHostsToDoc(infosExternes,this.xmlHostsFileName,elementNames);
			NodeList requestList = XPathAPI.selectNodeList(infosdoc,"//Layer");
			StringBuffer emptyLayers = new StringBuffer();
			int nbemptyLayers = 0;
			
			List<String> layersInfosXML = new ArrayList<String>();
			List<String> layersMinScales = new ArrayList<String>();
			List<String> layersMaxScales = new ArrayList<String>();
			List<String> layersId = new ArrayList<String>();
			List<String> layersMsgError = new ArrayList<String>();
	
			List<OwsResultAsynRequest> listOwsResultAsynRequest = new ArrayList<OwsResultAsynRequest>(); 
			
			for (int i=0 ; i < requestList.getLength() ; i++) {
				
				layerNameNode = XPathAPI.selectSingleNode(requestList.item(i),"Name");
				layerName = layerNameNode.getFirstChild().getNodeValue().replaceAll("&amp;","&").replaceAll("&#39;", "'");
				String layerMinScale = "null";
				String layerMaxScale = "null";
				String layerId = "null";
				try {
					layerMinScale = XPathAPI.selectSingleNode(requestList.item(i),"LayerMinScale").getFirstChild().getNodeValue();
				} catch (NullPointerException npe) {
					// tant pis
				} catch (TransformerException e) {
					// tant pis
				}
				try {
					layerMaxScale = XPathAPI.selectSingleNode(requestList.item(i),"LayerMaxScale").getFirstChild().getNodeValue();
				} catch (NullPointerException npe) {
					// tant pis
				} catch (TransformerException e) {
					// tant pis
				}
				try {
					layerId = XPathAPI.selectSingleNode(requestList.item(i),"LayerId").getFirstChild().getNodeValue();
				} catch (NullPointerException npe) {
					// tant pis
				} catch (TransformerException e) {
					// tant pis
				}
				layerRequestNodes = XPathAPI.selectNodeList(requestList.item(i),"Request");
				layerVersionNodes = XPathAPI.selectNodeList(requestList.item(i),"Version");
				layerFeatureGeometryNameNodes = XPathAPI.selectNodeList(requestList.item(i),"FeatureGeometryName");
				//String fluxInfos = "";
				
				OwsResultAsynRequest owsResultAsynRequest = new OwsResultAsynRequest();
				owsResultAsynRequest.setLayerMinScale(layerMinScale);
				owsResultAsynRequest.setLayerMaxScale(layerMaxScale);
				owsResultAsynRequest.setLayerId(layerId);
				owsResultAsynRequest.setLayerName(layerName);
				
				for(int iNode=0 , len=layerRequestNodes.getLength() ; iNode<len ; iNode++) {
	
					layerRequest = layerRequestNodes.item(iNode).getFirstChild().getNodeValue().replaceAll("&amp;","&").replace("+", "%2B");
					if(layerVersionNodes.item(iNode) != null){
						layerVersion = layerVersionNodes.item(iNode).getFirstChild().getNodeValue();
					}
					
					if(layerFeatureGeometryNameNodes.item(iNode) != null){
						layerFeatureGeometryName = layerFeatureGeometryNameNodes.item(iNode).getFirstChild().getNodeValue();
					}
					
					if (body.get("gmlMask") != null) {
						layerRequest += "&FILTER=" + URLEncoder.encode("(<Filter><Intersects><PropertyName>" + layerFeatureGeometryName + "</PropertyName>" + body.get("gmlMask") + "</Intersects></Filter>)", "UTF-8");
					}
					if (body.get("dataMask") != null) {
						layerRequest += "&FILTER=" + URLEncoder.encode("(<Filter>" + body.get("dataMask") + "</Filter>)", "UTF-8");
					}
					if (body.get("pixelMask") != null) {
						String pixel = body.get("pixelMask").replaceAll("#", "&");
						if(layerVersion.equalsIgnoreCase("WMS 1.3")|| layerVersion.equalsIgnoreCase("WMS 1.3.0")){
							pixel=pixel.replaceAll("X", "I").replaceAll("Y", "J");
						}
						layerRequest += pixel;
					}
					LOGGER.debug("***** OwsHelperService doPost layerRequest: " + layerRequest);
					owsResultAsynRequest.getLayerRequests().add(layerRequest);

				}
				
				listOwsResultAsynRequest.add(owsResultAsynRequest);

			}

			List<CompletableFuture<OwsResultAsynRequest>> contentFutures = listOwsResultAsynRequest.stream()
			        .map(resultAsynRequest -> sendAsynRequest(resultAsynRequest, this.httpProxy, this.requestHeaders))
			        .collect(Collectors.toList());

			
			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
			        contentFutures.toArray(new CompletableFuture[contentFutures.size()])
			);
			
			CompletableFuture<List<OwsResultAsynRequest>> allContentsFuture = allFutures.thenApply(v -> {
			   return contentFutures.stream()
			           .map(contentFuture -> contentFuture.join())
			           .collect(Collectors.toList());
			});
			
			List<OwsResultAsynRequest> resultAsynRequests = allContentsFuture.get();
			
			for (OwsResultAsynRequest resultAsynRequest : resultAsynRequests) {
				if (resultAsynRequest.getFluxInfos().equals("") && this.outputFormat.equals("text/html")) {
					emptyLayers.append("<br/>" + resultAsynRequest.getLayerName());
					nbemptyLayers++;
				} else {
					layersInfosXML.add("<FeatureCollection name=\"" + resultAsynRequest.getLayerName().replaceAll("&","&amp;").replaceAll("\\\\","&#92;&#92;&#92;").replaceAll("\"","\\\\&quot;")+ "\">" + resultAsynRequest.getFluxInfos().replaceAll("\r\n","\n").replaceAll("\\\\\\\\","&#92;").replaceAll("<featureMember/>", "")+ "</FeatureCollection>"); // 
					layersMinScales.add(resultAsynRequest.getLayerMinScale());
					layersMaxScales.add(resultAsynRequest.getLayerMaxScale());
					layersId.add(resultAsynRequest.getLayerId());
					layersMsgError.add(resultAsynRequest.getLayerMsgError());
				}
			}

			String outputString = "";
			if (this.outputFormat.equals("text/html")) {
				outputString += "<html><head><title>Informations</title><style>" + this.getCssStyles() + "</style></head><body class=\"resultat\">";
				if (body.get("withCsvExport") != null) {
					outputString += "<form action=\"#\" method=\"post\"><input type=\"hidden\" name=\"CSVcontent\" id=\"CSVcontent\" value=\"\"/></form>";
				}

				for (int i=0, len=layersInfosXML.size() ; i<len ; i++) {
					String htmlTable = "";
					if (this.xsl2HtmlFile == null) {
						htmlTable += XmlTools.executeInternalXSL(layersInfosXML.get(i), new ClassPathResource(this.internalXsl2HtmlFile, this.getClass().getClassLoader()).getInputStream(), this.getXsltParams(layersMinScales.get(i), layersMaxScales.get(i), body.get("withReturn"), layersId.get(i), layersMsgError.get(i)));
					} else {
						htmlTable += XmlTools.executeXSL(layersInfosXML.get(i), this.xsl2HtmlFile, this.getXsltParams(layersMinScales.get(i), layersMaxScales.get(i), body.get("withReturn"), layersId.get(i), layersMsgError.get(i)));
					}
					outputString += htmlTable;
					if (body.get("withCsvExport") != null) {
						outputString += "<br/><div align=\"center\"><input type=\"submit\" csvExport=\"true\" value=\"Export CSV\" /></div><br/>";
					}
				}
				if (nbemptyLayers!=0) {
					outputString += "<span class=\"entete\">"
									+ "<br/>Aucun objet trouvé pour "
									+ ((nbemptyLayers==1) ? "le thème suivant" : "les thèmes suivants") + " :"
									+ emptyLayers + "</span>";
				}
				outputString += "</body></html>";
				
			} else if (this.outputFormat.equals("text/plain")) {
				outputString = "[";
				for (int i=0, len=layersInfosXML.size() ; i<len ; i++) {
					String json = XmlTools.executeInternalXSL(layersInfosXML.get(i), new ClassPathResource(this.internalXsl2JsonFile, this.getClass().getClassLoader()).getInputStream(),  this.getXsltParams(layersMinScales.get(i), layersMaxScales.get(i), body.get("withReturn"), layersId.get(i), layersMsgError.get(i)));
					if (!outputString.equals("[")) {
						outputString += ",";
					}
					outputString += json;
				}
				outputString += "]";
				
			} else if (this.outputFormat.equals("text/xml")) {
				outputString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
				for (int i=0, len=layersInfosXML.size() ; i<len ; i++) {
					outputString += layersInfosXML.get(i);
				}
			}
			
			message = outputString;
			LOGGER.debug("***** OwsHelperService doPost outputString: " + outputString);
			
		} catch (HostMappingException e) {
			//response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
			LOGGER.error("***** OwsHelperService doPost HostMappingException: " + e);
		} catch (ParserConfigurationException e) {
			//response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
			LOGGER.error("***** OwsHelperService doPost ParserConfigurationException: " + e);
		} catch (SAXException e) {
			//response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
			LOGGER.error("***** OwsHelperService doPost SAXException: " + e);
		} catch (TransformerException e) {
			//response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
			LOGGER.error("***** OwsHelperService doPost TransformerException: " + e);
		} catch (InterruptedException e) {
			//response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
			LOGGER.error("***** OwsHelperService doPost InterruptedException: " + e);
		} catch (ExecutionException e) {
			//response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
			LOGGER.error("***** OwsHelperService doPost ExecutionException: " + e);
		}	
		
		LOGGER.debug("***** OwsHelperService Fin doPost");
		
		//return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
		return message;		
	}
	
    public CompletableFuture<OwsResultAsynRequest> sendAsynRequest(OwsResultAsynRequest owsResultAsynRequest, HttpProxy httpProxy, List<Header> requestHeaders) {
    	return CompletableFuture.supplyAsync(() -> {
    		LOGGER.debug("***** OwsHelperService sendAsynRequest *****");
			String getFeatureXxxResultXML = null;
			String fluxInfos = "";
    		try {
    			List<String> layerRequests = owsResultAsynRequest.getLayerRequests();
    			for (String layerRequest : layerRequests) {
    				LOGGER.debug("***** OwsHelperService url: " + layerRequest);
    				getFeatureXxxResultXML = HttpSender.getGetResponse(layerRequest,httpProxy,this.httpConnectionTimeout,this.httpSocketTimeout,this.requestHeaders);
    				fluxInfos += XmlTools.executeInternalXSL(getFeatureXxxResultXML, new ClassPathResource(this.internalXslFile, this.getClass().getClassLoader()).getInputStream(), null);
    				
    			}	
			} catch (HttpSenderException e) {
				// ne rien faire, le flux est vide
				LOGGER.error("***** OwsHelperService HttpSenderException" + e);
				owsResultAsynRequest.setLayerMsgError(this.formatMessageError(""+e.getCause()));
			} catch (TransformerException e) {
				// ne rien faire, le flux est vide
				LOGGER.error("***** OwsHelperService TransformerException" + e);
				owsResultAsynRequest.setLayerMsgError(this.formatMessageError(""+e.getCause()));
			} catch (IOException e) {
				// ne rien faire, le flux est vide
				LOGGER.error("***** OwsHelperService IOException" + e);
				owsResultAsynRequest.setLayerMsgError(this.formatMessageError(""+e.getCause()));
			}

    		owsResultAsynRequest.setFluxInfos(fluxInfos);
	        return owsResultAsynRequest;
    	});
    }
	
	private Hashtable<String, String> getXsltParams(String layerMinScale, String layerMaxScale, String withReturn, String layerId, String layerMsgError) {
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("layerMinScale", layerMinScale);
		params.put("layerMaxScale", layerMaxScale);
		params.put("layerId", layerId);
		params.put("layerMsgError", layerMsgError);
		if (withReturn != null) {
			params.put("withReturn", withReturn);
		}
		if (this.imageLocateMenu != null) {
			params.put("pictoLocateMenu", this.imageLocateMenu);
		}
		if (this.imageLocateItem != null) {
			params.put("pictoLocate", this.imageLocateItem);
		}
		return params;
	}
	
	private String getCssStyles() throws IOException {
		StringBuffer stylesCssBuf = new StringBuffer();
		LineNumberReader stylesCssReader;
		if (this.stylesCssFileName == null) {
			stylesCssReader = new LineNumberReader(new InputStreamReader(new ClassPathResource(INTERNAL_STYLE_CSS, this.getClass().getClassLoader()).getInputStream()));
		} else {
			stylesCssReader = new LineNumberReader(new FileReader(this.stylesCssFileName));
		}
		String stylesCssLine = stylesCssReader.readLine();
		while (stylesCssLine != null) {
			stylesCssBuf.append(stylesCssLine + "\n");
			stylesCssLine = stylesCssReader.readLine();
		}
		stylesCssReader.close();

		return stylesCssBuf.toString();
	}

	private String formatMessageError(String error) {
		String msg = "Erreur: ";
		if (error.indexOf("TimeoutException") != -1) {
			//ERREUR: java.net.SocketTimeoutException: Read timed out
			msg += "La couche n'a pas répondu dans le délai imparti - " + error;
		} else if (error.indexOf("TransformerException") != -1 && error.indexOf("attribut") != -1) {
			//ERREUR: javax.xml.transform.TransformerException: com.sun.org.apache.xml.internal.utils.WrappedRuntimeException: Le type d'élément org_38038_33b25354-1bec-4164-90cf-62616fd5909d:D doit être suivi des spécifications d'attribut, > ou />.
			msg += "La réponse est illisible, certains attributs interrogés contiennent des caractères interdits - " + error;
		} else if (error.indexOf("TransformerException") != -1) {
			//ERREUR: javax.xml.transform.TransformerException: com.sun.org.apache.xml.internal.utils.WrappedRuntimeException: Contenu non autorisé dans le prologue
			msg += "La réponse est illisible - " + error;
		} else {
			//autre cas d'erreur
			msg += "L'interrogation de la couche a échoué - " + error;
		}
		msg = msg.replaceAll("\"","");
		return msg;
	}
}
