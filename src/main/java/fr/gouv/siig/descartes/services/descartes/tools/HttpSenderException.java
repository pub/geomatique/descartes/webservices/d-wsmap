package fr.gouv.siig.descartes.services.descartes.tools;

/**
 * Class: descartes.tools.HttpSenderException
 * Exception déclenchée lors des appels HTTP internes.
 * 
 * Hérite de:
 * - java.lang.Exception
 */
public class HttpSenderException extends Exception {

	private static final long serialVersionUID = 5905428886129867559L;

	/**
	 * Constructeur: HttpSenderException()
	 * Construteur vide.
	 */
	public HttpSenderException() {
        super();
    }

	/**
	 * Constructeur: HttpSenderException(String)
     * Constructeur selon le message.
     * 
     * Paramètres:
	 * message - {String} Message
	 */
    public HttpSenderException(String message) {
        super(message);
    }

	/**
	 * Constructeur: HttpSenderException(Throwable)
     * Constructeur selon l'exception source.
     * 
     * Paramètres:
	 * exp - {Throwable} Exception source
	 */
    public HttpSenderException(Throwable exp) {
        super(exp);
    }

	/**
	 * Constructeur: HttpSenderException(String, Throwable)
     * Constructeur selon le message et l'exception source.
     * 
     * Paramètres:
	 * message - {String} Message
	 * exp - {Throwable} Exception source
	 */
    public HttpSenderException(String message, Throwable exp) {
        super(message, exp);
    }
}
