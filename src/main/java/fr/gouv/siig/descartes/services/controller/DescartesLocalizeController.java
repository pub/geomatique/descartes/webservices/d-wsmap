package fr.gouv.siig.descartes.services.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;

import fr.gouv.siig.descartes.services.descarteslocalize.ListeService;

@Controller
public class DescartesLocalizeController {
	
	private static final Logger LOGGER = LogManager.getLogger(DescartesLocalizeController.class);
	
	@Autowired
	public ListeService listeService;
	
	@CrossOrigin
    @GetMapping(value="/api/v1/localize")
	public void contextManager2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller localize GET");
		this.listeService.doGet(request, response);
		LOGGER.debug("***** FIN controller localize GET");
	}

}
