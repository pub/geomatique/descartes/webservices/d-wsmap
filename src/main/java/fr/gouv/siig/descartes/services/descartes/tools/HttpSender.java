package fr.gouv.siig.descartes.services.descartes.tools;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.xml.sax.SAXException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class: descartes.tools.HttpSender
 * Classe utilitaire d'exécution de requêtes HTTP.
 */
public class HttpSender {
	
	private static final Logger LOGGER = LogManager.getLogger(HttpSender.class);
	
    private HttpSender() {
    	// magic
    }
    
	/**
	 * Staticmethode: getPostResponseAsByte(String, String, HttpProxy)
	 * Retourne la réponse à une requête POST.
	 * 
	 * Paramètres:
	 * serverURL - {String} URL du serveur HTTP
	 * requestBody - {String} Corps de la requête
	 * httpProxy - {<HttpProxy>} Proxy
	 * 
	 * Retour:
	 * {byte[]} Réponse à la requête
	 * 
	 * Lance:
	 * {<HttpSenderException>} - En cas d'impossibilité de communiquer avec le serveur contacté.
	 */
    public static byte[] getPostResponseAsByte(String serverURL, String requestBody, HttpProxy httpProxy) throws HttpSenderException {
    	byte[] result = null;
        HttpClient client = new HttpClient();
        if (HttpSender.hasProxy(serverURL, httpProxy)) {
        	client.getHostConfiguration().setProxy(httpProxy.getHost(), httpProxy.getPort());
        }
        PostMethod post = null;
        try {
            post = new PostMethod(serverURL);
            post.setRequestBody(requestBody);
            client.executeMethod(post);
            result = post.getResponseBody();
        } catch (HttpException e) {
        	LOGGER.error("***** HttpSender HttpException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } catch (IOException e) {
        	LOGGER.error("***** HttpSender IOException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } finally {
        	post.releaseConnection();
        }
        return result;
      }

	/**
	 * Staticmethode: getGetResponseAsByte(String, String, HttpProxy)
	 * Retourne la réponse à une requête GET.
	 * 
	 * Paramètres:
	 * serverURL - {String} URL du serveur HTTP
	 * requestBody - {String} Corps de la requête
	 * httpProxy - {<HttpProxy>} Proxy
	 * 
	 * Retour:
	 * {byte[]} Réponse à la requête
	 * 
	 * Lance:
	 * {<HttpSenderException>} - En cas d'impossibilité de communiquer avec le serveur contacté.
	 */
    public static byte[] getGetResponseAsByte(String serverURL, String requestBody, HttpProxy httpProxy) throws HttpSenderException {
    	byte[] result = null;
        HttpClient client = new HttpClient();
        if (HttpSender.hasProxy(serverURL, httpProxy)) {
        	client.getHostConfiguration().setProxy(httpProxy.getHost(), httpProxy.getPort());
        }
        GetMethod get = null;
        try {
            get = new GetMethod(serverURL + requestBody);
            client.executeMethod(get);
            result = get.getResponseBody();
        } catch (HttpException e) {
        	LOGGER.error("***** HttpSender HttpException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } catch (IOException e) {
        	LOGGER.error("***** HttpSender IOException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } finally {
        	get.releaseConnection();
        }
        return result;
      }

	/**
	 * Staticmethode: getGetResponseAsByte(String, HttpProxy)
	 * Retourne la réponse à une requête GET.
	 * 
	 * Paramètres:
	 * globalURL - {String} URL de la requête
	 * httpProxy - {<HttpProxy>} Proxy
	 * 
	 * Retour:
	 * {byte[]} Réponse à la requête
	 * 
	 * Lance:
	 * {<HttpSenderException>} - En cas d'impossibilité de communiquer avec le serveur contacté.
	 */
    public static byte[] getGetResponseAsByte(String globalURL, HttpProxy httpProxy) throws HttpSenderException {
    	byte[] result = null;
        HttpClient client = new HttpClient();
        if (HttpSender.hasProxy(globalURL, httpProxy)) {
        	client.getHostConfiguration().setProxy(httpProxy.getHost(), httpProxy.getPort());
        }
        GetMethod get = null;
        try {
            get = new GetMethod(globalURL);
            client.executeMethod(get);
            result = get.getResponseBody();
        } catch (HttpException e) {
        	LOGGER.error("***** HttpSender HttpException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } catch (IOException e) {
        	LOGGER.error("***** HttpSender IOException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } finally {
        	get.releaseConnection();
        }
        return result;
      }

	/**
	 * Staticmethode: getPostResponse(String, StringBuffer, HttpProxy)
	 * Retourne la réponse à une requête POST.
	 * 
	 * Paramètres:
	 * serverURL - {String} URL du serveur HTTP
	 * requestBody - {StringBuffer} Corps de la requête
	 * httpProxy - {<HttpProxy>} Proxy
	 * 
	 * Retour:
	 * {String} Réponse à la requête
	 * 
	 * Lance:
	 * {<HttpSenderException>} - En cas d'impossibilité de communiquer avec le serveur contacté.
	 */
    public static String getPostResponse(String serverURL, StringBuffer requestBody, HttpProxy httpProxy) throws HttpSenderException {
    	String result = "";
        HttpClient client = new HttpClient();
        if (HttpSender.hasProxy(serverURL, httpProxy)) {
        	client.getHostConfiguration().setProxy(httpProxy.getHost(), httpProxy.getPort());
        }
        PostMethod post = null;
        try {
            post = new PostMethod(serverURL);
            post.setRequestBody(requestBody.toString());
            client.executeMethod(post);
            result = post.getResponseBodyAsString();
        } catch (HttpException e) {
        	LOGGER.error("***** HttpSender HttpException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } catch (IOException e) {
        	LOGGER.error("***** HttpSender IOException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } finally {
        	post.releaseConnection();
		}
        return result;
      }

	/**
	 * Staticmethode: getGetResponse(String, HttpProxy)
	 * Retourne la réponse à une requête GET.
	 * 
	 * Paramètres:
	 * globalURL - {String} URL de la requête
	 * httpProxy - {<HttpProxy>} Proxy
	 * 
	 * Retour:
	 * {String} Réponse à la requête
	 * 
	 * Lance:
	 * {<HttpSenderException>} - En cas d'impossibilité de communiquer avec le serveur contacté.
	 */
    public static String getGetResponse(String globalURL, HttpProxy httpProxy, int httpConnectionTimeout, int httpSocketTimeout, List<Header> requestHeaders) throws HttpSenderException {
    	String result = "";
    	
        HttpClient client = new HttpClient();
        client.getParams().setParameter("http.socket.timeout", httpConnectionTimeout);
        client.getParams().setParameter("http.connection.timeout", httpSocketTimeout);
        if (HttpSender.hasProxy(globalURL, httpProxy)) {
        	LOGGER.debug("***** HttpSender getGetResponse hasProxy: " + httpProxy.getHost() + " " + httpProxy.getPort());
        	client.getHostConfiguration().setProxy(httpProxy.getHost(), httpProxy.getPort());
        }
        GetMethod get = null;
        
		try {
			get = new GetMethod(globalURL);
			for (int i = 0 ; i < requestHeaders.size(); i++) {
				LOGGER.debug("***** HttpSender getGetResponse header: " + requestHeaders.get(i).getName() + " " + requestHeaders.get(i).getValue());
				if (requestHeaders.get(i).getName().equalsIgnoreCase("X-origine.UtilisateurProvenance")) {
					LOGGER.debug("***** HttpSender getGetResponse header: " + requestHeaders.get(i).getName() + " " + requestHeaders.get(i).getValue());
					get.addRequestHeader(requestHeaders.get(i).getName(), requestHeaders.get(i).getValue());
				}
			}
            client.executeMethod(get);
            result = new String(get.getResponseBody(), "UTF-8");

        } catch (HttpException e) {
        	LOGGER.error("***** HttpSender HttpException " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } catch (IOException e) {
        	LOGGER.error("***** HttpSender IOException " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } finally {
        	get.releaseConnection();
        }
        return unDeclareDTD(result);
    }

	/**
	 * Staticmethode: getGetResponseAsStream(String, HttpProxy)
	 * Retourne la réponse à une requête GET.
	 * 
	 * Paramètres:
	 * globalURL - {String} URL de la requête
	 * httpProxy - {<HttpProxy>} Proxy
	 * 
	 * Retour:
	 * {InputStream} Réponse à la requête
	 * 
	 * Lance:
	 * {<HttpSenderException>} - En cas d'impossibilité de communiquer avec le serveur contacté.
	 */
    public static InputStream getGetResponseAsStream(String globalURL, HttpProxy httpProxy) throws HttpSenderException {
        InputStream result = null;
        HttpClient client = new HttpClient();
        if (HttpSender.hasProxy(globalURL, httpProxy)) {
        	client.getHostConfiguration().setProxy(httpProxy.getHost(), httpProxy.getPort());
        }
        GetMethod get = null;
        try {
            get = new GetMethod(globalURL);
            client.executeMethod(get);
            result = get.getResponseBodyAsStream();
        } catch (HttpException e) {
        	LOGGER.error("***** HttpSender HttpException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } catch (IOException e) {
        	LOGGER.error("***** HttpSender IOException: " + e);
        	throw new HttpSenderException("Impossible de communiquer avec le serveur",e);
        } finally {
        	get.releaseConnection();
        }
        return result;
      }
      
    private static boolean hasProxy(String globalURL, HttpProxy httpProxy) throws HttpSenderException {
    	  boolean proxy = false;
    	  if (httpProxy != null && httpProxy.getHost() != null && !httpProxy.getHost().equalsIgnoreCase("") && httpProxy.getPort()!=0) {
    		  proxy = true;
              LOGGER.debug("***** HttpSender hasProxy urlName: " + globalURL);
    		  try {
    			  if (httpProxy.getUrlExceptions() != null && !httpProxy.getUrlExceptions().equalsIgnoreCase("")) {
    				  LOGGER.debug("***** HttpSender hasProxy httpProxy.getUrlExceptions(): " + httpProxy.getUrlExceptions());
    				  URL url = new URL(globalURL);				  
    				  String host = url.getHost();
    				  LOGGER.debug("***** HttpSender hasProxy host: " + host);
    				  String[] noProxyURLs = httpProxy.getUrlExceptions().split(";");
    				  for (String noProxyURL : noProxyURLs) {
    					  LOGGER.debug("***** HttpSender hasProxy noProxyURL: " + noProxyURL);
    					  if (host.startsWith(noProxyURL) || host.endsWith(noProxyURL)) {
    						  proxy = false;
    						  break;
    					  }
    				  }
    			  }
    		  } catch (MalformedURLException e) {
    			  LOGGER.error("***** HttpSender hasProxy MalformedURLException: " + globalURL, e);
    			  throw new HttpSenderException("URL non valide : " + globalURL,e);
    		  }
    	  }
    	  LOGGER.debug("***** HttpSender hasProxy Proxy config: " + proxy);
    	  return proxy;
      }
      
      private static String unDeclareDTD(String content) {
  	    StringBuffer buf = new StringBuffer();
	    int ouvrants = 1;
	    boolean closed = false;
	    int index;
	    char car;

	    int beginDTD = content.indexOf("<!DOCTYPE");
	    if (beginDTD != -1) {
	    	buf.append(content.substring(0,beginDTD-1));
	    	content = content.substring(beginDTD);
	    	index = beginDTD;
	    	while (!closed) {
	    		index++;
	    		car = content.charAt(index);
	    		if (car == '<') {
	    			ouvrants ++;
	    		}
	    		if (car == '>') {
	    			ouvrants --;
	    		}
	    		closed = (ouvrants == 0);
	    	}
	    	buf.append(content.substring(index+1));
	    } else {
	    	buf.append(content);
	    }
	    return buf.toString();
	  }
      
      private static String fixWithInnerEncoding(String content) throws ParserConfigurationException, SAXException, IOException {
    	  String result = null;
    	  String encoding = XmlTools.getInnerEncoding(content);
    	  result = new String(content.getBytes(), encoding);
    	  
    	  return result;
      }
}
