package fr.gouv.siig.descartes.services.descartes.services;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xpath.XPathAPI;

/**
 * Class: descartes.services.HostMapping
 * Classe utilitaire pour les correspondances entre Hosts internes au centre-serveur et Hosts publics.
 */
public class HostMapping {
	
	private HostMapping() {
		// magic
	}

	/**
	 * Staticmethode: mapHostsToDoc(String, String, String[])
	 * Transforme un flux XML contenant les Hosts publics en arbre DOM contenant les Hosts internes.
	 * 
	 * Paramètres:
	 * flux	- {String} Flux XML contenant les Hosts publics
	 * xmlMapping - {String} Nom du fichier contenant les correspondances
	 * elementNames	- {String[]} Noms des éléments du flux XML devant faire l'objet des correspondances
	 * 
	 * Retour:
	 * {Document} Arbre DOM contenant les Hosts internes
	 * 
	 * Lance:
	 * {<HostMappingException>} - Si une exception est déclenchée
	 */
	public static Document mapHostsToDoc(String flux, String xmlMapping, String[] elementNames) throws HostMappingException {
		Document doc = null;
		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(flux)));
		} catch (SAXException e) {
			throw new HostMappingException(e);
		} catch (IOException e) {
			throw new HostMappingException(e);
		} catch (ParserConfigurationException e) {
			throw new HostMappingException(e);
		}

		return mapHostsToDoc(doc, xmlMapping, elementNames);
	}

	/**
	 * Staticmethode: mapHostsToString(String, String, String[])
	 * Transforme un flux XML contenant les Hosts publics en flux XML contenant les Hosts internes.
	 * 
	 * Paramètres:
	 * flux	- {String} Flux XML contenant les Hosts publics
	 * xmlMapping - {String} Nom du fichier contenant les correspondances
	 * elementNames	- {String[]} Noms des éléments du flux XML devant faire l'objet des correspondances
	 * 
	 * Retour:
	 * {String} Flux XML contenant les Hosts internes
	 * 
	 * Lance:
	 * {<HostMappingException>} - Si une exception est déclenchée
	 */
	public static String mapHostsToString(String flux, String xmlMapping, String[] elementNames) throws HostMappingException {
		StringWriter sw = new StringWriter();
		try {
			XMLSerializer serializer = new XMLSerializer(sw, new OutputFormat("XML","UTF-8",true));
			serializer.serialize(mapHostsToDoc(flux, xmlMapping, elementNames));
		} catch (IOException e) {
			throw new HostMappingException(e);
		}
		
		return sw.toString();
	}
	
	/**
	 * Staticmethode: mapHostsToDoc(Document, String, String[])
	 * Transforme un arbre DOM contenant les Hosts publics en arbre DOM contenant les Hosts internes.
	 * 
	 * Paramètres:
	 * tree	- {Document} Arbre DOM contenant les Hosts publics
	 * xmlMapping - {String} Nom du fichier contenant les correspondances
	 * elementNames	- {String[]} Noms des éléments du flux XML devant faire l'objet des correspondances
	 * 
	 * Retour:
	 * {Document} Arbre DOM contenant les Hosts internes
	 * 
	 * Lance:
	 * {<HostMappingException>} - Si une exception est déclenchée
	 */
	public static Document mapHostsToDoc(Document tree, String xmlMapping, String[] elementNames) throws HostMappingException {
		try {
			if (xmlMapping != null) {
				Document map = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlMapping);
				
				for (String elementName : elementNames) {
					NodeList nodes = XPathAPI.selectNodeList(tree,"//"+elementName);
					for (int i=0 ; i<nodes.getLength() ; i++) {
						String url = nodes.item(i).getFirstChild().getNodeValue();
						Node nodeInterne = XPathAPI.selectSingleNode(map,"//Interne[contains('" + url + "',../Externe)]");
						if (nodeInterne!=null) {
							Node nodeExterne = XPathAPI.selectSingleNode(nodeInterne,"../Externe");
							Node qsa = XPathAPI.selectSingleNode(nodeInterne,"../QSA");
							String newURL = url.replaceAll(nodeExterne.getFirstChild().getNodeValue(),nodeInterne.getFirstChild().getNodeValue());
							if (qsa != null) {
								newURL += "&" + qsa.getFirstChild().getNodeValue();
							}
							nodes.item(i).getFirstChild().setNodeValue(newURL);
						}
					}
				}
			}
		} catch (SAXException e) {
			throw new HostMappingException(e);
		} catch (IOException e) {
			throw new HostMappingException(e);
		} catch (ParserConfigurationException e) {
			throw new HostMappingException(e);
		} catch (TransformerException e) {
			throw new HostMappingException(e);
		}
		return tree;
	}
	
	/**
	 * Staticmethode: mapHostsToString(String, String)
	 * Transforme une URL ayant un Host public en URL ayant un Host interne.
	 * 
	 * Paramètres:
	 * url	- {String} URL ayant un Host public
	 * xmlMapping - {String} Nom du fichier contenant les correspondances
	 * 
	 * Retour:
	 * {String} URL ayant un Host public
	 * 
	 * Lance:
	 * {<HostMappingException>} - Si une exception est déclenchée
	 */
	public static String mapHostToString(String url, String xmlMapping) throws HostMappingException {
		try {
			if (xmlMapping != null) {
				Document map = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlMapping);
				Node nodeInterne = XPathAPI.selectSingleNode(map,"//Interne[contains('" + url + "',../Externe)]");
				if (nodeInterne!=null) {
					Node nodeExterne = XPathAPI.selectSingleNode(nodeInterne,"../Externe");
					Node qsa = XPathAPI.selectSingleNode(nodeInterne,"../QSA");
					url = url.replaceAll(nodeExterne.getFirstChild().getNodeValue(),nodeInterne.getFirstChild().getNodeValue());
					if (qsa != null) {
						url += "&" + qsa.getFirstChild().getNodeValue();
					}
				}
			}
		} catch (SAXException e) {
			throw new HostMappingException(e);
		} catch (IOException e) {
			throw new HostMappingException(e);
		} catch (ParserConfigurationException e) {
			throw new HostMappingException(e);
		} catch (TransformerException e) {
			throw new HostMappingException(e);
		}
		return url;
	}
}
