package fr.gouv.siig.descartes.services.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import fr.gouv.siig.descartes.services.descartes.services.ContextManagerService;

@Controller
public class DescartesContextController {
	
	private static final Logger LOGGER = LogManager.getLogger(DescartesContextController.class);
	
	@Autowired
	public ContextManagerService contextManagerService;
	
	@CrossOrigin
    @PostMapping(value="/api/v1/contextManager")
	public void contextManager(@RequestParam Map<String, String> body, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller contextManager POST");
		LOGGER.debug("view: " + body.get("view"));
		LOGGER.debug("context: " + body.get("context"));
		this.contextManagerService.doPost(body, response);	
		LOGGER.debug("***** FIN controller contextManager POST");

	}
	
	@CrossOrigin
    @GetMapping(value="/api/v1/contextManager")
	public void contextManager2(@RequestParam Map<String, String> params, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOGGER.debug("***** DEBUT controller contextManager GET");
		this.contextManagerService.doGet(params, request, response);
		LOGGER.debug("***** FIN controller contextManager GET");
	}

}
