package fr.gouv.siig.descartes.services.descartes.services;

import java.util.ArrayList;
import java.util.List;

public class OwsResultAsynRequest {

	private List<String> layerRequests;
	private String fluxInfos;
	private String layerMinScale;
	private String layerMaxScale;
	private String layerId;
	private String layerName;
	private String layerMsgError;
	
	public OwsResultAsynRequest () {
		this.layerRequests = new ArrayList<String>();
		this.fluxInfos = "";
		this.layerMinScale = "";
		this.layerMaxScale = "";
		this.layerId = "";
		this.layerName = "";
		this.layerMsgError = "";
	}
	
	public List<String> getLayerRequests() {
		return layerRequests;
	}
	
	public void setLayerRequests(List<String> layerRequests) {
		this.layerRequests=layerRequests;
	}
	
	public String getFluxInfos() {
		return fluxInfos;
	}
	
	public void setFluxInfos(String fluxInfos) {
		this.fluxInfos=fluxInfos;
	}

	public String getLayerMinScale() {
		return layerMinScale;
	}
	
	public void setLayerMinScale(String layerMinScale) {
		this.layerMinScale=layerMinScale;
	}
	
	public String getLayerMaxScale() {
		return layerMaxScale;
	}
	
	public void setLayerMaxScale(String layerMaxScale) {
		this.layerMaxScale=layerMaxScale;
	}
	
	public String getLayerId() {
		return layerId;
	}
	
	public void setLayerId(String layerId) {
		this.layerId=layerId;
	}
	
	public String getLayerName() {
		return layerName;
	}
	
	public void setLayerName(String layerName) {
		this.layerName=layerName;
	}

	public String getLayerMsgError() {
		return layerMsgError;
	}
	
	public void setLayerMsgError(String layerMsgError) {
		this.layerMsgError=layerMsgError;
	}
}
