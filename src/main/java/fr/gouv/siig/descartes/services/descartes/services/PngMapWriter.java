package fr.gouv.siig.descartes.services.descartes.services;

import fr.gouv.siig.descartes.services.descartes.tools.HttpProxy;
import fr.gouv.siig.descartes.services.descartes.tools.HttpSender;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.ByteArrayInputStream;

import javax.imageio.ImageIO;

/**
 * Class: descartes.services.PngMapWriter
 * Classe du thread de génération PNG.
 * 
 * Implémente:
 * - java.lang.Runnable
 * 
 * Hérite de:
 * - <descartes.services.AbstractMapWriter>
 */
public class PngMapWriter extends AbstractMapWriter implements Runnable {
	private static final String fontName = "Dialog";
	private static final int fontStyle = Font.BOLD;
	private static final int fontSize = 10;
	private static final Font fontInfos = new Font(PngMapWriter.fontName,PngMapWriter.fontStyle,PngMapWriter.fontSize);

	/**
	 * Constructeur: PngMapWriter(HttpProxy)
	 * Constructeur d'instances
	 * 
	 * Paramètres:
	 * proxy - {<descartes.tools.HttpProxy>} Proxy.
	 */
	public PngMapWriter(HttpProxy proxy) {
		super(proxy);
	}

	/**
	 * Methode: run()
	 * Exécute le thread.
	 */
	public void run() {
		
		try {
			if (this.urlCartes.length>0) {
				this.pas = 100/(this.urlCartes.length);
			}
			BufferedImage imageFinale = new BufferedImage(this.largeurPixCarte,this.hauteurPixCarte,BufferedImage.TYPE_INT_RGB);
			Graphics2D graphicImageFinale = imageFinale.createGraphics();
	
			graphicImageFinale.setColor(Color.WHITE);
			graphicImageFinale.fillRect(0,0,this.largeurPixCarte,this.hauteurPixCarte);
			this.avancement = 1;
			for (int indexImage=0 ; indexImage<this.urlCartes.length ; indexImage++) {
				if (!this.urlCartes[indexImage].equals("KO")) {
					byte[] dbBytes = HttpSender.getGetResponseAsByte(this.urlCartes[indexImage], this.httpProxy);
					ByteArrayInputStream bais = new ByteArrayInputStream(dbBytes);
					BufferedImage imageCouche = ImageIO.read(bais);
					BufferedImage imageCoucheTemp = new BufferedImage(imageCouche.getWidth(null), imageCouche.getHeight(null), BufferedImage.TYPE_INT_ARGB);
					Graphics graphicImageCoucheTemp = imageCoucheTemp.getGraphics();
					graphicImageCoucheTemp.drawImage(imageCouche, 0, 0, null);
					float[] scales = { 1f, 1f, 1f, this.opacites[indexImage] };
					float[] offsets = new float[4];
					RescaleOp rop = new RescaleOp(scales, offsets, null);
					graphicImageFinale.drawImage(imageCoucheTemp, rop, 0, 0);

					this.avancement += this.pas;
				}
			}
			
			graphicImageFinale.setBackground(Color.WHITE);
			graphicImageFinale.setFont(PngMapWriter.fontInfos);

			FontMetrics fontMetrics = graphicImageFinale.getFontMetrics(PngMapWriter.fontInfos);
			if (this.longueurEchelle != 0) {
				int petiteLongueur = (int) this.longueurEchelle/4;
				graphicImageFinale.setColor(Color.WHITE);
				graphicImageFinale.fillRect(10, this.hauteurPixCarte-15, petiteLongueur, 5);
				graphicImageFinale.setColor(Color.BLACK);
				graphicImageFinale.fillRect(10 + petiteLongueur, this.hauteurPixCarte-15, petiteLongueur, 5);
				graphicImageFinale.setColor(Color.WHITE);
				graphicImageFinale.fillRect(10 + 2*petiteLongueur, this.hauteurPixCarte-15, petiteLongueur, 5);
				graphicImageFinale.setColor(Color.BLACK);
				graphicImageFinale.fillRect(10 + 3*petiteLongueur, this.hauteurPixCarte-15, petiteLongueur, 5);
				graphicImageFinale.drawRect(10, this.hauteurPixCarte-15, 4*petiteLongueur, 5);
	        	int largeurTexteEchelle = fontMetrics.stringWidth(this.affichageEchelle);
	        	int piedTexte = fontMetrics.getDescent();
	        	int hauteurTexte = fontMetrics.getHeight();
	        	graphicImageFinale.setColor(Color.WHITE);
				graphicImageFinale.fillRect(10 + 4*petiteLongueur + 10, this.hauteurPixCarte-(15-5) - (hauteurTexte - piedTexte), largeurTexteEchelle, hauteurTexte);
	        	graphicImageFinale.setColor(Color.BLACK);
				graphicImageFinale.drawString(this.affichageEchelle, 10 + 4*petiteLongueur + 10, this.hauteurPixCarte-(15-5));
			}

			if (this.copyrightCarte != null) {
	        	int largeurCopyright = fontMetrics.stringWidth("©" + this.copyrightCarte);
	        	int piedTexte = fontMetrics.getDescent();
	        	int hauteurTexte = fontMetrics.getHeight();
	        	graphicImageFinale.setColor(Color.WHITE);
				graphicImageFinale.fillRect(10, 15 - (hauteurTexte - piedTexte), largeurCopyright, hauteurTexte);
	        	graphicImageFinale.setColor(Color.BLACK);
	        	graphicImageFinale.drawString("©" + this.copyrightCarte, 10, 15);
	        }

			imageFinale.flush();
			this.avancement = 100;
			ImageIO.write(imageFinale, "png", this.getWriter());
			this.getWriter().flush();
			this.getWriter().close();
		} catch (Exception e) {
			this.erreur = e;
			this.avancement = -1;
		}
	}
}
