package fr.gouv.siig.descartes.services.descartes.tools;

/**
 * Class: descartes.tools.HttpProxy
 * Classe "bean" représentant les proxies nécessaires selon les contraintes d'hébergement.
 */
public class HttpProxy {
	
	private String host;
	private int port = 0;
	private String urlExceptions;
	
	/**
	 * Constructeur: HttpProxy(String, port)
	 * Constructeur selon le Host et le Port du proxy.
	 * 
	 * Paramètres:
	 * host - {String} Host du proxy
	 * port - {int} Port du proxy
	 */
	public HttpProxy (String host, int port) {
		this.host = host;
		this.port = port;
	}

	/**
	 * Constructeur: HttpProxy(String, String, String)
	 * Constructeur selon le Host, le Port et les exceptions d'URL du proxy.
	 * 
	 * Paramètres:
	 * host - {String} Host du proxy
	 * port - {String} Port du proxy
	 * urlExceptions - {String} Exceptions d'URL
	 */
	public HttpProxy (String host, String port, String urlExceptions) {
		this.host = host;
		if (port != null) {
			this.port = Integer.valueOf(port);
		}
		this.urlExceptions = urlExceptions;
	}

	/**
	 * Methode: getHost()
	 * Fournit le host du proxy
	 * 
	 * Retour:
	 * {String} Le host du proxy
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Methode: getPort()
	 * Fournit le port du proxy
	 * 
	 * Retour:
	 * {int} Le port du proxy
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Methode: getUrlExceptions()
	 * Fournit les exceptions d'URL du proxy
	 * 
	 * Retour:
	 * {String} Les exceptions d'URL du proxy
	 */
	public String getUrlExceptions() {
		return urlExceptions;
	}

}
