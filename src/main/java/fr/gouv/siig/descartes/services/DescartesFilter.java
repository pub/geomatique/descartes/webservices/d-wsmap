package fr.gouv.siig.descartes.services;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;

@WebFilter("*")
public class DescartesFilter implements Filter {

	@Value("${server.id}")
	private String serverHostId;
	
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, 
      FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        serverHostId = serverHostId.split("\\.")[0];
        httpServletResponse.setHeader(
          "D-Server-Id", serverHostId);
        chain.doFilter(request, response);
    }
}
