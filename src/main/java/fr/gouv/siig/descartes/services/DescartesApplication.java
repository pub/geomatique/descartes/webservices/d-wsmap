package fr.gouv.siig.descartes.services;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.filter.ForwardedHeaderFilter;

@ServletComponentScan
@SpringBootApplication
public class DescartesApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(DescartesApplication.class, args);
	}

	//actuator
	@Bean
	public FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilter() {
	    final FilterRegistrationBean<ForwardedHeaderFilter> filter = new FilterRegistrationBean<>(new ForwardedHeaderFilter());
	    filter.setName("Forwarded Header filter");
	    filter.setUrlPatterns(Collections.singletonList("/manage/*"));
	    return filter;
	}
	

    /*@Bean
    public HttpTraceRepository httpTraceRepository() {
        return new InMemoryHttpTraceRepository();
    }*/

}
