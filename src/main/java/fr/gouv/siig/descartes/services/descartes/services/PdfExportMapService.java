package fr.gouv.siig.descartes.services.descartes.services;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.gouv.siig.descartes.services.descartes.tools.PapersManager;

/**
 * Class: descartes.services.PdfExportMapServlet
 * Servlet de génération d'exports PDF.
 * 
 * Hérite de:
 * - <descartes.services.ExportMapServlet>
 */
@Service
public class PdfExportMapService extends ExportMapService{

	@Value("${app.descartes.global.proxy.host}")
	private String proxyHost;
	@Value("${app.descartes.global.proxy.port}")
	private String proxyPort;
	@Value("${app.descartes.global.proxy.exceptions}")
	private String proxyExceptions;
	@Value("${app.descartes.global.hostMapping}")
	private String hostmapping;
	@Value("${app.descartes.exportPDF.extraPapers}")
	private String exportPdfExtraPapers;
	@Value("${app.descartes.exportPDF.repertoireLogos}")
	private String exportPdfRepertoireLogos;
	@Value("${app.descartes.exportPDF.tailleMaxLogo}")
	private String exportPdfTailleMaxLogo;
	
	private static final Logger LOGGER = LogManager.getLogger(PdfExportMapService.class);
	
	protected static final String extraPapers = "extraPapers";
	
	public PdfExportMapService() {
		LOGGER.info("********** PdfExportMapService **********");
	}

	/**
	 * Methode: init()
	 * Initialise le service avec le format PDF et les Paramètres du servlet :
	 * 
	 * :
	 * repertoireLogos - Eventuel répertoire des logos stockés sur le serveur d'application.
	 * tailleMaxLogo - Eventuel la taille maximale d affichage des logos.
	 * extraPapers - Eventuel fichier de définition des formats de papier complémentaires.
	 * 
	 * Exemple de configuration du servlet dans le descripteur de déploiement:
	 * (start code)
	 * 	<servlet>
	 * 		<servlet-name>exportPDF</servlet-name>
	 * 		<servlet-class>fr.gouv.siig.descartes.services.descartes.services.PdfExportMapServlet</servlet-class>
	 * 		<init-param>
	 * 			<param-name>repertoireLogos</param-name>
	 * 			<param-value>opt/data/descartes/logos/</param-value>
	 * 		</init-param>
	 * 		<init-param>
	 *			<param-name>tailleMaxLogo</param-name>
	 *			<param-value>64</param-value>
	 * 		</init-param>
	 * 		<init-param>
	 * 			<param-name>extraPapers</param-name>
	 * 			<param-value>WEB-INF/papers.properties</param-value>
	 * 		</init-param>
	 * 	</servlet>
	 * (end)
	 */
	@PostConstruct
	public void init(){
		this.exportFileFormat = ExportMapService.EXPORT_PDF;
		this.contentType = ExportMapService.CONTENT_TYPE_PDF;
		if (exportPdfRepertoireLogos != null && !"".equals(exportPdfRepertoireLogos)) {
			this.logoDirectory = exportPdfRepertoireLogos;
		}
		if (exportPdfTailleMaxLogo != null && !"".equals(exportPdfTailleMaxLogo)) {
			this.logoSizeMax = exportPdfTailleMaxLogo;
		}
		try {
			if (exportPdfExtraPapers != null && !"".equals(exportPdfExtraPapers)) {
				PapersManager.initPapers(exportPdfExtraPapers);
			}
		} catch (Exception e) {
			// l'exception est ignoree car le fichier des formats est optionnel.
			// l'exception a prendre en compte sera lancee ulterieurement lors de la tentative d'acces au format souhaite
		}
		super.init(proxyHost, proxyPort, proxyExceptions, hostmapping);
		LOGGER.info("********** PdfExportMapService OK **********");
	}

}
