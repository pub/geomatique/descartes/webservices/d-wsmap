package fr.gouv.siig.descartes.services.controller;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.CrossOrigin;

import fr.gouv.siig.descartes.services.descartes.services.GetFeatureInfoService;
import fr.gouv.siig.descartes.services.descartes.services.GetFeatureService;

@Controller
public class DescartesGetFeatureController {
	
	private static final Logger LOGGER = LogManager.getLogger(DescartesGetFeatureController.class);
	
	@Autowired
	public GetFeatureService getFeatureService;
	
	@Autowired
	public GetFeatureInfoService getFeatureInfoService;
	
	@CrossOrigin
    @PostMapping(value="/api/v1/getFeature")
    @ResponseBody
	public String getfeature(@RequestParam Map<String, String> body, @RequestHeader Map<String,String> headers) throws Exception {
		LOGGER.debug("***** DEBUT controller getFeature");
		String response = this.getFeatureService.doPost(body, headers);
		LOGGER.debug("***** FIN controller getFeature");
		return response;		
	}
	
	@CrossOrigin
    @PostMapping(value="/api/v1/getFeatureInfo")
    @ResponseBody
	public String getfeatureinfo(@RequestParam Map<String, String> body, @RequestHeader Map<String,String> headers) throws Exception {
		LOGGER.debug("***** DEBUT controller getFeatureInfo");
		String response = this.getFeatureInfoService.doPost(body, headers);
		LOGGER.debug("***** FIN controller getFeatureInfo");
		return response;	
	}
	
}
