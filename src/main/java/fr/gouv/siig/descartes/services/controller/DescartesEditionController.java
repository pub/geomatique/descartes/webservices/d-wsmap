package fr.gouv.siig.descartes.services.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.CrossOrigin;

import fr.gouv.siig.descartes.services.descartesedition.EditionGeoJSONService;
import fr.gouv.siig.descartes.services.descartesedition.EditionKMLService;
import fr.gouv.siig.descartes.services.descartesedition.EditionService;

@Controller
public class DescartesEditionController {
	
	private static final Logger LOGGER = LogManager.getLogger(DescartesEditionController.class);
	
	@Autowired
	public EditionService editionService;
	
	@Autowired
	public EditionGeoJSONService editionGeoJSONService;
	
	@Autowired
	public EditionKMLService editionKMLService;
	
	@CrossOrigin
    @PostMapping(value="/api/v1/edition")
    @ResponseBody
    public void edition(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		LOGGER.debug("***** DEBUT controller edition");
		this.editionService.doPost(request, response);		
		LOGGER.debug("***** FIN controller edition");
	}
	
	@CrossOrigin
    @PostMapping(value="/api/v1/editionGeoJSON")
    @ResponseBody
    public void editionGeoJSON(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		LOGGER.debug("***** DEBUT controller editionGeoJSON");
		this.editionGeoJSONService.doPost(request, response);		
		LOGGER.debug("***** FIN controller editionGeoJSON");
	}
	
	@CrossOrigin
    @PostMapping(value="/api/v1/editionKML")
    @ResponseBody
    public void editionKML(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		LOGGER.debug("***** DEBUT controller editionKML");
		this.editionKMLService.doPost(request, response);		
		LOGGER.debug("***** FIN controller editionKML");
	}

}
