package fr.gouv.siig.descartes.services.descartesedition;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

@Service
public class EditionKMLService {

	@Value("${app.descartes.editionKML.kmldir}")
	private String editionKMLkmldir;
	
	private static final Logger LOGGER = LogManager.getLogger(EditionKMLService.class);
	
	protected String kmlDir = null;

	public EditionKMLService() {
		LOGGER.info("********** EditionKMLService **********");
	}

	@PostConstruct
	public void init() {
		LOGGER.info("*** editionKMLkmldir: " + editionKMLkmldir);
		if (editionKMLkmldir != null && !editionKMLkmldir.equals("")) {
			this.kmlDir =  editionKMLkmldir;
			if (!this.kmlDir.endsWith("/")) {
				this.kmlDir += "/";
			}
		}

		LOGGER.info("*** kmlDir: " + this.kmlDir);
		LOGGER.info("********** EditionKMLService OK **********");
	}
	
	public void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doSomething(request, response);
	}

	public void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doSomething(request, response);
	}

	public void doSomething(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		StringBuffer buffer = new StringBuffer();
		String line = null;
		JSONObject jsonFlux;
		String fileUrl;
		String content;
		int status = 200;

		DocumentBuilderFactory domFactory;
		DocumentBuilder domBuilder = null;
		Document domAdd;
		Document domUpdated;
		Document domRemoved;
		Document domFile;

		String message = "DESCARTES n'est pas chargé de faire la sauvegarde des objets géographiques.</br></br>Ici, pour les tests, la sauvegarde a été faite par du code particulier côté serveur.";

		try {

			BufferedReader reader = req.getReader();
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}

			jsonFlux = (JSONObject) (new JSONParser()).parse(buffer.toString());

			fileUrl = jsonFlux.get("url").toString();
			fileUrl = this.kmlDir+fileUrl;
			LOGGER.debug("***** EditionKML fileUrl: " + fileUrl);
			content = jsonFlux.get("content").toString();
			LOGGER.debug("***** EditionKML content: " + content);
			if(!fileUrl.isEmpty() && !content.isEmpty()){
				
				JSONObject contentObject =  (JSONObject) jsonFlux.get("content");
				
				domFactory = DocumentBuilderFactory.newInstance();
				domBuilder = domFactory.newDocumentBuilder();
				
				domAdd = domBuilder.parse(new InputSource(new StringReader(contentObject.get("addObjects").toString())));
				domUpdated = domBuilder.parse(new InputSource(new StringReader(contentObject.get("updatedObjects").toString())));
				domRemoved = domBuilder.parse(new InputSource(new StringReader(contentObject.get("removedObjects").toString())));

				domFile = domBuilder.parse(fileUrl);

				NodeList addList = domAdd.getElementsByTagName("Placemark");
				NodeList updatedList = domUpdated.getElementsByTagName("Placemark");
				NodeList removedList = domRemoved.getElementsByTagName("Placemark");

				removePlacemark(domFile, removedList);
				removePlacemark(domFile, updatedList);
				addPlacemarks(domFile, updatedList, false);
				addPlacemarks(domFile, addList, false);

				byte[] data = DocumentToString(domFile).getBytes();
				LOGGER.debug("***** EditionKML fileUrl: " + fileUrl);
				Path file = Paths.get(fileUrl);
				LOGGER.debug("***** EditionKML file: " + file);
				Files.write(file, data);
				
			}  else {
      		  status=500;
			  message= message + "<br><br> Problème lors de la sauvegarde.";
			  LOGGER.error("***** EditionKML Problème lors de la sauvegarde.");
      	  	}
			

		} catch (Exception e) {
			status = 500;
			message = message + "<br><br> Problème lors de la sauvegarde.";
			LOGGER.error("***** EditionKML Problème lors de la sauvegarde:" + e);
		}
		
        String jsonReturned = "{\"status\":" + status + ",\"message\": \"" + message + "\"}";
		LOGGER.debug("***** EditionKML jsonReturned: " + jsonReturned);
		resp.setContentType("application/json");
		resp.setStatus(status);
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().print(new String(jsonReturned));
	}

	/**
	 * ajoute les placemarks d'une liste à un Document
	 * 
	 * @param initDoc
	 * @param toAdd
	 * @param newId
	 * @return
	 */
	private void addPlacemarks(Document initDoc, NodeList toAdd, boolean newId) {

		for (int i = 0; i < toAdd.getLength(); i++) {

			Node newNode = initDoc.importNode(toAdd.item(i), true);
			Node node = initDoc.getDocumentElement().getFirstChild();

			while (node != null && node.getNodeType() != Node.ELEMENT_NODE) {
				node = node.getNextSibling();
			}

			Element elem = (Element) node;
			elem.appendChild(newNode);

			if (newId) {
				((Element) newNode).setAttribute("id",
						String.valueOf((int) (Math.random() * 1000000)));

			}
		}

	}

	/**
	 * Enl�ve les placemarks d'une liste d'un Document
	 * 
	 * @param doc
	 * @param list
	 */
	private void removePlacemark(Document doc, NodeList list) {

		for (int i = 0; i < list.getLength(); i++) {

			Element node = (Element) list.item(i);
			String listId = node.getAttribute("id");

			if (listId != null) {

				NodeList nodes = doc.getElementsByTagName("Placemark");

				int j = nodes.getLength() - 1;

				while (j >= 0) {

					Element docElement = (Element) nodes.item(j);
					String docId = docElement.getAttribute("id");
					Node docNode = nodes.item(j);

					if (docId.equals(listId)) {
						docNode.getParentNode().removeChild(docNode);
					}

					j--;
				}
			}
		}
	}

	/**
	 * Convertit un document en String
	 * 
	 * @param doc
	 * @return
	 */
	public static String DocumentToString(Document doc) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ElementToStream(doc.getDocumentElement(), baos);
		return new String(baos.toByteArray());
	}

	public static void ElementToStream(Element element, OutputStream out) {
		try {
			DOMSource source = new DOMSource(element);
			StreamResult result = new StreamResult(out);
			TransformerFactory transFactory = TransformerFactory.newInstance();
			Transformer transformer = transFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(source, result);
		} catch (Exception ex) {
		}
	}

}
