package fr.gouv.siig.descartes.services.descartes.services;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Class: descartes.services.GetFeatureInfoServlet
 * Servlet d'accès aux données sémantiques de serveurs WMS.
 * 
 * Hérite de:
 * - <descartes.services.OwsHelperServlet>
 */
@Service
public class GetFeatureInfoService extends OwsHelperService {	
	
	@Value("${app.descartes.global.proxy.host}")
	private String proxyHost;
	@Value("${app.descartes.global.proxy.port}")
	private String proxyPort;
	@Value("${app.descartes.global.proxy.exceptions}")
	private String proxyExceptions;
	@Value("${app.descartes.global.hostMapping}")
	private String hostmapping;
	
	@Value("${app.descartes.getFeatureInfo.httpConnectionTimeout}")
	private int getfeatureinfoHttpconnectiontimeout;
	@Value("${app.descartes.getFeatureInfo.httpSocketTimeout}")
	private int getfeatureinfoHttpsockettimeout;
	@Value("${app.descartes.getFeatureInfo.stylesCSS}")
	private String getfeatureinfoStylescss;
	@Value("${app.descartes.getFeatureInfo.XSLfile}")
	private String getfeatureinfoXslfile;
	
	private static final Logger LOGGER = LogManager.getLogger(GetFeatureInfoService.class);
	
	private static final int DEFAULT_HTTP_CONNECTION_TIMEOUT = 10000; //ms
	private static final int DEFAULT_HTTP_SOCKET_TIMEOUT = 10000; //ms
	
	public GetFeatureInfoService() {
		LOGGER.info("********** GetFeatureInfoService **********");
	}

	/**
	 * Methode: init()
	 * Initialise le service avec les Paramètres :
	 * 
	 * :
	 * stylesCSS - Eventuel fichier de la feuille de style CSS présentant la réponse en flux HTML, remplaçant la feuille de style CSS par défaut 
	 * XSLfile - Eventuel fichier de transformation XSL-T formattant la réponse en flux HTML, remplaçant la transformation par défaut
	 * 
	 */
	@PostConstruct
	public void init() {
		this.internalXslFile = "resources/getXmlFeatureInfo.xsl";
		LOGGER.info("***** internalXslFile: " + this.internalXslFile);
		LOGGER.info("***** getfeatureinfoStylescss: " + getfeatureinfoStylescss);
		if (getfeatureinfoStylescss != null && !getfeatureinfoStylescss.equals("")) {
			this.stylesCssFileName =  getfeatureinfoStylescss;
		}
		LOGGER.info("***** stylesCssFileName: " + this.stylesCssFileName);
		LOGGER.info("***** getfeatureinfoXslfile: " + getfeatureinfoXslfile);
		if (getfeatureinfoXslfile != null && !getfeatureinfoXslfile.equals("")) {
			this.xsl2HtmlFile = getfeatureinfoXslfile;
		}
		LOGGER.info("***** xsl2HtmlFile: " + this.xsl2HtmlFile);
		this.httpConnectionTimeout = DEFAULT_HTTP_CONNECTION_TIMEOUT;
		if (getfeatureinfoHttpconnectiontimeout != 0) {
			this.httpConnectionTimeout = getfeatureinfoHttpconnectiontimeout;
		}
		LOGGER.info("***** httpConnectionTimeout: " + this.httpConnectionTimeout);
		this.httpSocketTimeout = DEFAULT_HTTP_SOCKET_TIMEOUT;
		if (getfeatureinfoHttpsockettimeout  != 0) {
			this.httpSocketTimeout = getfeatureinfoHttpsockettimeout;
		}
		LOGGER.info("***** httpSocketTimeout: " + this.httpSocketTimeout);
		super.init(proxyHost, proxyPort, proxyExceptions, hostmapping);
		LOGGER.info("********** GetFeatureInfoService OK **********");
	}

}
