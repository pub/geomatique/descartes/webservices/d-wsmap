package fr.gouv.siig.descartes.services.descartes.services;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Class: descartes.services.PngExportMapServlet
 * Servlet de génération d'exports PNG.
 * 
 * Hérite de:
 * - <descartes.services.ExportMapService>
 */
@Service
public class PngExportMapService extends ExportMapService {
	
	@Value("${app.descartes.global.proxy.host}")
	private String proxyHost;
	@Value("${app.descartes.global.proxy.port}")
	private String proxyPort;
	@Value("${app.descartes.global.proxy.exceptions}")
	private String proxyExceptions;
	@Value("${app.descartes.global.hostMapping}")
	private String hostmapping;
	
	private static final Logger LOGGER = LogManager.getLogger(PngExportMapService.class);

	public PngExportMapService() {
		LOGGER.info("********** PngExportMapService **********");
	}
	
	/**
	 * Methode: init()
	 * Initialise le service avec le format PNG.
	 * 
	 * Exemple de configuration du servlet dans le descripteur de déploiement:
	 * (start code)
	 * 	<servlet>
	 * 		<servlet-name>exportPNG</servlet-name>
	 * 		<servlet-class>fr.gouv.siig.descartes.services.descartes.services.PngExportMapServlet</servlet-class>
	 * 	</servlet>
	 * (end)
	 */
	@PostConstruct
	public void init(){
		this.exportFileFormat = ExportMapService.EXPORT_PNG;
		this.contentType = ExportMapService.CONTENT_TYPE_PNG;
		super.init(proxyHost, proxyPort, proxyExceptions, hostmapping);
		LOGGER.info("********** PngExportMapService OK **********");
	}

}
