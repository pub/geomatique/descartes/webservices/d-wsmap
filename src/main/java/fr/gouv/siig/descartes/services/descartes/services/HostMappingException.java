package fr.gouv.siig.descartes.services.descartes.services;

/**
 * Class: descartes.services.HostMappingException
 * Exception déclenchée lors de la convertion des Hosts publics en Hosts internes.
 * 
 * Hérite de:
 * - java.lang.Exception
 */
public class HostMappingException extends Exception {

	private static final long serialVersionUID = -3211603240025094376L;
	
	/**
	 * Constructeur: HostMappingException()
	 * Construteur vide.
	 */
	public HostMappingException() {
        super();
    }

	/**
	 * Constructeur: HostMappingException(String)
     * Constructeur selon le message.
     * 
     * Paramètres:
	 * message - {String} Message
	 */
    public HostMappingException(String message) {
        super(message);
    }

	/**
	 * Constructeur: HostMappingException(Throwable)
     * Constructeur selon l'exception source.
     * 
     * Paramètres:
	 * exp - {Throwable} Exception source
	 */
    public HostMappingException(Throwable exp) {
        super(exp);
    }

	/**
	 * Constructeur: HostMappingException(String, Throwable)
     * Constructeur selon le message et l'exception source.
     * 
     * Paramètres:
	 * message - {String} Message
	 * exp - {Throwable} Exception source
	 */
    public HostMappingException(String message, Throwable exp) {
        super(message, exp);
    }
}
