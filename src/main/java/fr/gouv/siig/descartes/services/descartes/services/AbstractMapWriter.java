package fr.gouv.siig.descartes.services.descartes.services;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.gouv.siig.descartes.services.descartes.tools.HttpProxy;

/**
 * Class: descartes.services.AbstractMapWriter
 * Classe abstraite des threads de génération PNG et PDF.
 * 
 * Implémente:
 * - java.lang.Runnable
 * 
 * Classe dérivées:
 * - <descartes.services.PdfMapWriter>
 * - <descartes.services.PngMapWriter>
 */
public abstract class AbstractMapWriter implements Runnable {

	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
	int avancement = 0;
	Exception erreur = null;
	String[] urlCartes = new String[0];
	String[] urlLegendes = new String[0];
	String[] fileLogos = new String[0];
	float[] opacites = new float[0];
	String[] urlLogos = new String[0];
	int sizeMaxLogo;

	int hauteurPixCarte = 400; 
	int largeurPixCarte = 600;
	String titreCarte = "";
	String copyrightCarte;
	String auteurCarte;
	String dateValiditeCarte;
	String descriptionCarte;
	String applicationInfos;
	String creator;
	String affichageEchelle = "";
	float longueurEchelle = 0;
	
	String pageFormat = "A4";
	String pageOrientation = "P";
	float pageMarginTop = 10;
	float pageMarginBottom = 10;
	float pageMarginLeft = 10;
	float pageMarginRight = 10;
	String pageUnits = "mm";

	HttpProxy httpProxy;
	
	int pas = 100;
	
	/**
	 * Constructeur: AbstractMapWriter(HttpProxy)
	 * Constructeur d'instances
	 * 
	 * Paramètres:
	 * proxy - {<descartes.tools.HttpProxy>} Proxy.
	 */
	public AbstractMapWriter(HttpProxy proxy) {
		this.httpProxy = proxy;
	}

	/**
	 * Methode: getTitreCarte()
	 * Fournit le titre de la carte.
	 * 
	 * Retour: 
	 * {String} Titre de la carte.
	 */
	public String getTitreCarte() {
		return titreCarte;
	}

	/**
	 * Methode: getAvancement()
	 * Fournit l'avancement du déroulement du thread.
	 * 
	 * Retour: 
	 * {int} Avancement du déroulement du thread (entre 1 et 100).
	 */
	public int getAvancement() {
		return avancement;
	}
	
	/**
	 * Methode: getErreur()
	 * Fournit l'exception levée lors du déroulement du thread.
	 * 
	 * Retour: 
	 * {Exception} Exception levée lors du déroulement du thread.
	 */
	public Exception getErreur() {
		return erreur;
	}

	/**
	 * Methode: getWriter()
	 * Fournit le lux binaire contenant le résultat de l'exportation.
	 * 
	 * Retour: 
	 * {ByteArrayOutputStream} Flux binaire contenant le résultat de l'exportation.
	 * 
	 * Lance:
	 * {Exception} - Si la génération du document n'est pas terminée. 
	 */
	public ByteArrayOutputStream getWriter() throws Exception {
		if (this.avancement!=0 && this.avancement < 100) {
			throw new Exception("Le document n'est pas encore fini!");
		}
		return baos;
	}

	/**
	 * Methode: setTitreCarte(String)
	 * Renseigne le titre de la carte.
	 * 
	 * Paramètres: 
	 * titreCarte - {String} Titre de la carte.
	 */
	public void setTitreCarte(String titreCarte) {
		this.titreCarte = titreCarte;
	}

	/**
	 * Methode: setHauteurPixCarte(int)
	 * Renseigne la hauteur en pixels de l'image de la carte.
	 * 
	 * Paramètres: 
	 * hauteurPixCarte - {int} Hauteur en pixels de l'image de la carte.
	 */
	public void setHauteurPixCarte(int hauteurPixCarte) {
		this.hauteurPixCarte = hauteurPixCarte;
	}

	/**
	 * Methode: setLargeurPixCarte(int)
	 * Renseigne la largeur en pixels de l'image de la carte.
	 * 
	 * Paramètres: 
	 * largeurPixCarte - {int} Largeur en pixels de l'image de la carte.
	 */
	public void setLargeurPixCarte(int largeurPixCarte) {
		this.largeurPixCarte = largeurPixCarte;
	}

	/**
	 * Methode: setCopyrightCarte(String)
	 * Renseigne le copyright associé à la carte.
	 * 
	 * Paramétres: 
	 * copyrightCarte - {String} Copyright associé à la carte.
	 */
	public void setCopyrightCarte(String copyrightCarte) {
		this.copyrightCarte = copyrightCarte;
	}

	/**
	 * Methode: setUrlCartes(String[])
	 * Renseigne les URLs des couches de la carte.
	 * 
	 * Paramètres: 
	 * urlCartes - {String[]} URLs des couches de la carte, selon les spécifications WMS/GetMap
	 */
	public void setUrlCartes(String[] urlCartes) {
		this.urlCartes = urlCartes;
		this.createEchelle();
	}

	/**
	 * Methode: setOpacites(float[])
	 * Renseigne les opacités des couches.
	 * 
	 * Paramètres: 
	 * opacites	- {float[]} Opacités des couches.
	 */
	public void setOpacites(float[] opacites) {
		this.opacites = opacites;
	}

	/**
	 * Methode: setUrlLegendes(String[])
	 * Renseigne les URLs des légendes de la carte (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * urlLegendes - {String[]} URLs des légendes de la carte, selon les spécifications WMS/GetLegendGraphics ou en ressources statiques.
	 */
	public void setUrlLegendes(String[] urlLegendes) {
		this.urlLegendes = urlLegendes;
	}

	/**
	 * Methode: setFileLogos(String[])
	 * Renseigne les noms des fichiers de logos à insérer (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * fileLogos - {String[]} Noms des fichiers de logos à insérer.
	 */
	public void setFileLogos(String[] fileLogos) {
		this.fileLogos = fileLogos;
	}

	/**
	 * Methode: setUrlLogos(String[])
	 * Renseigne les URL(s) des logos à insérer (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * urlLogos - {String[]} URL(s) des logos à insérer.
	 */
	public void setUrlLogos(String[] urlLogos) {
		this.urlLogos = urlLogos;
	}

	/**
	 * Methode: setCreator(String)
	 * Renseigne le nom de l'application génératrice de la carte (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * creator - {String} Nom de l'application génératrice de la carte.
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	/**
	 * Methode: setApplicationInfos(String)
	 * Renseigne les informations liées à l'application génératrice (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * applicationInfos - {String} Informations liées à l'application génératrice (ex : nom, version, etc.).
	 */
	public void setApplicationInfos(String applicationInfos) {
		this.applicationInfos = applicationInfos;
	}

	/**
	 * Methode: setAuteurCarte(String)
	 * Renseigne l'auteur de la carte (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * auteurCarte - {String} Auteur de la carte.
	 */
	public void setAuteurCarte(String auteurCarte) {
		this.auteurCarte = auteurCarte;
	}

	/**
	 * Methode: setDateValiditeCarte(String)
	 * Renseigne la date de la validité de la carte (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * dateValiditeCarte - {String} Date de la validité de la carte.
	 */
	public void setDateValiditeCarte(String dateValiditeCarte) {
		this.dateValiditeCarte = dateValiditeCarte;
	}

	/**
	 * Methode: setDescriptionCarte(String)
	 * Renseigne les éléments de description de la carte (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * descriptionCarte - {String} Eléments de description de la carte.
	 */
	public void setDescriptionCarte(String descriptionCarte) {
		this.descriptionCarte = descriptionCarte;
	}

	/**
	 * Methode: setPageFormat(String)
	 * Renseigne le format de la page (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * pageFormat - {String} Format de la page (ex: A3, A4, A5, etc.).
	 */
	public void setPageFormat(String pageFormat) {
		this.pageFormat = pageFormat;
	}

	/**
	 * Methode: setPageOrientation(String)
	 * Renseigne l'orientation de la page (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * pageOrientation - {String} Orientation de la page ('L' pour paysage, "P" pour portrait).
	 */
	public void setPageOrientation(String pageOrientation) {
		this.pageOrientation = pageOrientation;
	}

	/**
	 * Methode: setPageMarginTop(float)
	 * Renseigne la marge 'haut' de la page (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * pageMarginTop - {float} Marge 'haut' de la page.
	 */
	public void setPageMarginTop(float pageMarginTop) {
		this.pageMarginTop = pageMarginTop;
	}

	/**
	 * Methode: setPageMarginBottom(float)
	 * Renseigne la marge 'bas' de la page (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * pageMarginBottom - {float} Marge 'bas' de la page.
	 */
	public void setPageMarginBottom(float pageMarginBottom) {
		this.pageMarginBottom = pageMarginBottom;
	}

	/**
	 * Methode: setPageMarginLeft(float)
	 * Renseigne la marge 'gauche' de la page (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * pageMarginLeft - {float} Marge 'gauche' de la page.
	 */
	public void setPageMarginLeft(float pageMarginLeft) {
		this.pageMarginLeft = pageMarginLeft;
	}

	/**
	 * Methode: setPageMarginRight(float)
	 * Renseigne la marge 'droit' de la page (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * pageMarginRight - {float} Marge 'droit' de la page.
	 */
	public void setPageMarginRight(float pageMarginRight) {
		this.pageMarginRight = pageMarginRight;
	}

	/**
	 * Methode: setPageUnits(String)
	 * Renseigne l'unité de mesure pour la page (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * pageUnits - {String} Unité de mesure pour la page.
	 */
	public void setPageUnits(String pageUnits) {
		this.pageUnits = pageUnits;
	}
	
	/**
	 * Methode: setSizeMaxLogo(Integer)
	 * Renseigne la taille maximale d'affichage des logos (Seulement pour le PDF).
	 * 
	 * Paramètres: 
	 * sizeMaxLogo - {Integer} Taille du logo.
	 */
	public void setSizeMaxLogo(Integer sizeMaxLogo) {
		this.sizeMaxLogo = sizeMaxLogo;
	}


	/**
	 * Calcule la longueur de l'échelle graphique et le texte associé.
	 *
	 */
	private void createEchelle() {
		float[] bbox = null;
		if (this.urlCartes.length>0) {
			Pattern patternBbox = Pattern.compile("BBOX=-?[0-9]+(\\.[0-9]*)?,-?[0-9]+(\\.[0-9]*)?,-?[0-9]+(\\.[0-9]*)?,-?[0-9]+(\\.[0-9]*)?",Pattern.CASE_INSENSITIVE);
			Matcher matcherBbox = patternBbox.matcher(this.urlCartes[0]);
			if (matcherBbox.find()) {
				String bboxSub = matcherBbox.group();
				bbox = new float[4];
				String[] bboxString = bboxSub.split("=")[1].split(",");
				for (int i=0; i<4; i++) {
					bbox[i] = Float.parseFloat(bboxString[i]);
				}
				float largeurCarteTerrain = bbox[2]-bbox[0];
				float longueurEchelleTerrainMax = largeurCarteTerrain/3;
				int truncLog = (int)Math.log10(longueurEchelleTerrainMax);
				int multi = (int)Math.pow(10,truncLog-1);
				if (truncLog<4) {
					multi = (int)Math.pow(10,truncLog);
				}
				int chiffresSignificatifs = (int)(longueurEchelleTerrainMax/multi);
				while ((chiffresSignificatifs>4)&&(chiffresSignificatifs%4!=0)) {
					chiffresSignificatifs--;
				}
				int longueurEchelleTerrain = chiffresSignificatifs*multi;
				this.longueurEchelle = longueurEchelleTerrain*this.largeurPixCarte/largeurCarteTerrain;
				this.affichageEchelle =  (longueurEchelleTerrain>1000)?((longueurEchelleTerrain/1000)+" km"):(longueurEchelleTerrain+" m");
			}
		}
	}
}
