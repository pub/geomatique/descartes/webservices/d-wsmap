#!/bin/sh

echo "**********************"
echo "*** INITIALISATION ***"
echo "**********************"

INIT_DATA_DIR="/tmp/init-data"
DATA_DIR="/opt/data"

initData () { 
  cp -R $INIT_DATA_DIR/* $DATA_DIR
  chmod -R 777 $DATA_DIR
  echo "[INFO] Le répertoire ${DATA_DIR} a été initialisé avec le répertoire ${INIT_DATA_DIR}."
  ls -al $DATA_DIR
}

if [ -d "$DATA_DIR" ]; 
then
  echo "[INFO] Le répertoire ${DATA_DIR} existe déjà."
  if [ -z "$(ls -A ${DATA_DIR})" ]; then
    initData
  else
    ls -al $DATA_DIR
  fi
else
  echo "[INFO] Le répertoire ${DATA_DIR} n'existe pas."
  if [ -d "$INIT_DATA_DIR" ];
  then
    mkdir $DATA_DIR
    initData
  else
    echo "[ERROR] Le répertoire ${DATA_DIR} n'a pas été initialisé car le répertoire ${INIT_DATA_DIR} n'existe pas."
  fi

fi
echo "**********************"
env
curl --version
echo "**********************"

exec "$@"
